package collins.tristan.stORM

class BaseEnum(val name: String) {
    override def toString = name
}

class BaseEnumOrd(val ord: Int, name: String) extends BaseEnum(name)

trait Enum[T <: BaseEnum] {
    def values: Set[T]

    final def get(name: String): Option[T] = {
        values.find(_.name == name)
    }
}

trait OrdEnum[T <: BaseEnumOrd] {
    def values: Set[T]

    final def get(ord: Int): Option[T] = {
        values.find(_.ord == ord)
    }

    final implicit def enum2Ord(enum: T): Int = enum.ord
}