package collins.tristan.stORM.converters

import collins.tristan.stORM.source.{IntVal, SourceValue, StringVal, ValueConverter}
import collins.tristan.stORM.{BaseEnum, BaseEnumOrd, Enum, OrdEnum}

/**
 * Created by Tristan on 15-Apr-15.
 */
final class EnumConverter[T <: BaseEnum](enum: Enum[T]) extends ValueConverter[T] {
    override implicit def toValue(from: T): SourceValue = from.name

    override implicit def fromValue(value: SourceValue): Option[T] = value match {
        case StringVal(s) => enum.get(s)
        case _ => None
    }
}

final class OrdEnumConverter[T <: BaseEnumOrd](enum: OrdEnum[T]) extends ValueConverter[T] {
    override implicit def toValue(from: T): SourceValue = from.ord

    override implicit def fromValue(value: SourceValue): Option[T] = value match {
        case IntVal(i) => enum.get(i)
        case StringVal(s) => enum.get(s.toInt)
        case _ => None
    }
}