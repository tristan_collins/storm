package collins.tristan.stORM

import java.util.UUID

import collins.tristan.stORM.source.{LongVal, SourceValue, StringVal, ValueConverter}
import org.joda.time.LocalDate

/**
 * Created by Tristan on 16-Apr-15.
 */
package object converters {
    implicit object LocalDateConverter extends ValueConverter[LocalDate] {
        override implicit def toValue(from: LocalDate): SourceValue = from.toDate.getTime

        override implicit def fromValue(value: SourceValue): Option[LocalDate] = value match {
            case LongVal(l) => Some(new LocalDate(l))
            case _ => None
        }
    }

    implicit object UUIDConverter extends ValueConverter[UUID] {
        implicit def str2UUID(value: String): UUID = UUID.fromString(value)

        implicit def uuid2Str(key: UUID): String = key.toString

        override implicit def fromValue(sVal: SourceValue): Option[UUID] = sVal match {
            case StringVal(s) => Some(s)
            case _ => None
        }

        override implicit def toValue(id: UUID): SourceValue = StringVal(id)
    }
}
