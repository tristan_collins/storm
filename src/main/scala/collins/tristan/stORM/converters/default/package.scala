package collins.tristan.stORM.converters

import collins.tristan.stORM.source.{SourceValue, ValueConverter}

import scala.reflect.ClassTag

/**
 * Created by Tristan on 16-Apr-15.
 */
package object default {
    sealed abstract class DefaultConverter[T: ClassTag] extends ValueConverter[T] {
        private val tag = implicitly[ClassTag[T]]
        protected val runtime = tag.runtimeClass

        private val converter = (tag.runtimeClass match {
            case x if x == classOf[Int] || x == classOf[Integer] => SourceValue.int2Value _
            case x if x == classOf[Boolean] => SourceValue.bool2Value _
            case x if x == classOf[Long] => SourceValue.long2Value _
            case x if x == classOf[String] => SourceValue.string2Value _
            case x if x == classOf[Byte] => SourceValue.byte2Value _
            case x if x == classOf[Double] => SourceValue.double2Value _
            case x if x == classOf[Float] => SourceValue.float2Value _
        }).asInstanceOf[T => SourceValue]

        final override implicit def toValue(from: T): SourceValue = converter(from)
        override implicit def fromValue(value: SourceValue): Option[T] = {
            try {
                if (value.value.getClass == runtime) Some(value.value.asInstanceOf[T])
                else None
            } catch {
                case _: Throwable => None
            }
        }
    }

    implicit object IntConverter extends DefaultConverter[Int] {
        override implicit def fromValue(value: SourceValue): Option[Int] = {
            val parsed = super.fromValue(value)

            if (parsed.isDefined) parsed
            else {
                val conClass = value.value.getClass

                if (conClass == classOf[String] || conClass == classOf[java.lang.String]) {
                    val str = value.value.asInstanceOf[String]
                    Some(str.toInt)
                }
                else {
                    val c = value.value.asInstanceOf[{def toInt: Int}]
                    Some(c.toInt)
                }
            }
        }
    }

    implicit object BooleanConverter extends DefaultConverter[Boolean] {
        override implicit def fromValue(value: SourceValue): Option[Boolean] = {
            val parsed = super.fromValue(value)

            if (parsed.isDefined) parsed
            else {
                val converted = value.value
                val conClass = converted.getClass

                if (conClass == runtime) Some(converted.asInstanceOf[Boolean])
                else {
                    try {
                        conClass match {
                            case x if x == classOf[Int] || x == classOf[Integer] => {
                                val xInt = converted.asInstanceOf[Int]
                                if (xInt == 0) Some(false) else Some(true)
                            }
                            case x if x == classOf[String] => Some(converted.asInstanceOf[String].toBoolean)
                            case _ => None
                        }
                    } catch {
                        case _: Throwable => None
                    }
                }
            }
        }
    }

    implicit object LongConverter extends DefaultConverter[Long] {
        override implicit def fromValue(value: SourceValue): Option[Long] = {
            val parsed = super.fromValue(value)
            if (parsed.isDefined) parsed
            else try {
                Some(parsed.asInstanceOf[{def toLong: Long}].toLong)
            } catch {
                case _: Throwable => None
            }
        }
    }

    implicit object StringConverter extends DefaultConverter[String]

    implicit object ByteConverter extends DefaultConverter[Byte] {
        override implicit def fromValue(value: SourceValue): Option[Byte] = {
            val parsed = super.fromValue(value)
            if (parsed.isDefined) parsed
            else try {
                Some(parsed.asInstanceOf[{def toByte: Byte}].toByte)
            } catch {
                case _: Throwable => None
            }
        }
    }

    implicit object DoubleConverter extends DefaultConverter[Double] {
        override implicit def fromValue(value: SourceValue): Option[Double] = {
            val parsed = super.fromValue(value)
            if (parsed.isDefined) parsed
            else try {
                Some(parsed.asInstanceOf[{def toDouble: Double}].toDouble)
            } catch {
                case _: Throwable => None
            }
        }
    }

    implicit object FloatConverter extends DefaultConverter[Float] {
        override implicit def fromValue(value: SourceValue): Option[Float] = {
            val parsed = super.fromValue(value)
            if (parsed.isDefined) parsed
            else try {
                Some(parsed.asInstanceOf[{def toFloat: Float}].toFloat)
            } catch {
                case _: Throwable => None
            }
        }
    }
}
