package collins.tristan.stORM.source

abstract class ValueConverter[From] {
    implicit def toValue(from: From): SourceValue
    implicit def fromValue(value: SourceValue): Option[From]
}