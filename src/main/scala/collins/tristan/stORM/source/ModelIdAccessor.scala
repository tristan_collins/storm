package collins.tristan.stORM.source

import collins.tristan.stORM.dao.{Dao, Identifiable}

import scala.reflect.ClassTag

trait ModelIdAccessor {
    def idOf[T: ClassTag](model: T, src: Requester[_, _]): Option[SourceValue]
    def modelWithId[T: ClassTag](id: SourceValue, src: Requester[_, _]): Option[T with Identifiable[_]]
}

case class Requester[Id, Model](id: Id, model: Model, dao: Dao[Id, Model])