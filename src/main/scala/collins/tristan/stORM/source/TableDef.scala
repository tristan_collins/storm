package collins.tristan.stORM.source

import java.lang.reflect.{Constructor, Method}
import java.util.logging.{Level, Logger}

import collins.tristan.stORM.dao.{Database, Identifiable}
import net.sf.cglib.proxy._

import scala.collection.mutable
import scala.reflect.ClassTag
import scala.reflect.runtime.{universe => ru}
import scala.util.{Failure, Success, Try}

abstract class TableDef[Model: ru.TypeTag: ClassTag, PK: ClassTag] {
    private type Row = Map[String, String]
    private val aliases = mutable.HashMap[String, String]()
    private var _parent: Option[TableDef[_ >: Model, _]] = None

    def id = "id"

    final val modelType = implicitly[ClassTag[Model]]
    final val idType = implicitly[ClassTag[PK]]

    final def tableName = implicitly[ClassTag[Model]].runtimeClass.getSimpleName
    final def tableParent = _parent
    private def tableParent_=[Parent >: Model](parentTable: TableDef[Parent, _]): Unit = _parent = Some(parentTable)

    private def getMatching[T <: Column[Model, _, _]: ClassTag]: Map[String, T] = {
        val colClass = implicitly[ClassTag[T]].runtimeClass

        val matchingColumns = List(
            this.getClass.getMethods
                .filter(m => m.getReturnType == colClass && m.getParameterCount == 0)
                .map(m => m.getName -> m.invoke(this).asInstanceOf[T]),
            this.getClass.getFields
                .filter(f => f.getType == colClass)
                .map(f => f.getName -> f.get(this).asInstanceOf[T])
        ).flatten

        matchingColumns.map({case (name, col) =>
            if (aliases.contains(col.fieldName)) aliases(col.fieldName) -> col
            else name -> col
        }).toMap
    }

    final lazy val columns = getMatching[MappedColumn[Model, _]]
    final lazy val optionalColumns = getMatching[OptionalColumn[Model, _]]
    final lazy val foreignKeys = getMatching[ForeignKey[Model, _]]
    final lazy val optionalForeignKeys = getMatching[OptionalForeignKey[Model, _]]
    final lazy val foreignSets = getMatching[ForeignSet[Model, Any]] ++ getMatching[JoinedSet[Model, Any, _]]

    final private def setAlias(col: Column[Model, _, _], alias: String): Unit = aliases += col.fieldName -> alias

    implicit class ColAlias[Field: ru.TypeTag: ClassTag, Col: ru.TypeTag: ClassTag]
        (col: Column[Model, Field, Col]) {
        def as(alias: String): Unit = setAlias(col, alias)
    }

    final protected def foreignKey[T: ru.TypeTag: ClassTag](field: String, table: TableDef[T, _]) =
        new ForeignKey[Model, T](nameOf(field), field, fkType = table.idType)

    final protected def optionalForeignKey[T: ru.TypeTag: ClassTag](field: String, table: TableDef[T, _]) =
        new OptionalForeignKey[Model, T](nameOf(field), field, fkType = table.idType)

    final protected def column[T: ClassTag](field: String)(implicit c: ValueConverter[T]) =
        new MappedColumn[Model, T](nameOf(field), field)

    final protected def column[T: ClassTag](implicit c: ValueConverter[T]): (ClassTag[T], ValueConverter[T]) = (implicitly[ClassTag[T]], c)
    final protected def map[T](col: (ClassTag[T], ValueConverter[T], String)) = {
        implicit val tag = col._1
        implicit val c = col._2
        new MappedColumn[Model, T](nameOf(col._3), col._3)
    }

    final protected def optional[T: ClassTag](field: String)(implicit c: ValueConverter[T]) =
        new OptionalColumn[Model, T](nameOf(field), field)

    final protected def oneToMany[T: ru.TypeTag: ClassTag](field: String, table: TableDef[T, _])
                                                          (fk: table.type => ForeignKey[T, Model]) =
        new ForeignSet[Model, T](nameOf(field), field, table, fk.asInstanceOf[TableDef[T, _] => ForeignKey[T, Model]])

    final protected def setParent[Parent >: Model](parentTable: TableDef[Parent, _]): Unit = tableParent = parentTable

    final protected def join[Join: ru.TypeTag: ClassTag, Child: ru.TypeTag: ClassTag, ChildPK: ru.TypeTag: ClassTag]
        (field: String, joinTable: JoinTable[Join, _, Model, PK, Child, ChildPK]) = {
        new JoinedSet[Model, Child, Join](nameOf(field), field, joinTable)
    }

    private final def nameOf(fieldName: String): String = {
        if (aliases.contains(fieldName)) aliases(fieldName)
        else fieldName
    }

    protected[stORM] final def nameOf(column: Column[Model, _, _]): String = nameOf(column.fieldName)

    implicit class MappedColumnBuilder[Field](args: (ClassTag[Field], ValueConverter[Field])) {
        def to(fieldName: String) = (args._1, args._2, fieldName)
    }
}

abstract class JoinTable[Join: ru.TypeTag: ClassTag, PK: ru.TypeTag: ClassTag, Parent, ParentPK, Child, ChildPK]
    (val parentTable: TableDef[Parent, ParentPK], val childTable: TableDef[Child, ChildPK])
    extends TableDef[Join, PK] {
    def parentId: ForeignKey[Join, Parent]
    def childId: ForeignKey[Join, Child]
}

sealed abstract class Column[Model: ru.TypeTag: ClassTag, Field: ClassTag, TableCol: ClassTag](colName: => String) {
    type FieldType = Field

    final val columnTag = implicitly[ClassTag[Field]]

    def fieldName: String
    final lazy val name = colName

    protected lazy val getter: Model => Field = {
        val method = ReflectionHelper.getMethodDec[Model](fieldName)

        if (method.isSuccess)
            model => ReflectionHelper.getMethod(method.get)(model).get.apply(model).asInstanceOf[Field]
        else {
            val field = ReflectionHelper.getFieldDec[Model](fieldName)
            model => ReflectionHelper.getField(field.get)(model).get.get.asInstanceOf[Field]
        }
    }

    protected[source] lazy val setter: (Model, Field) => Unit = {
        val field = ReflectionHelper.getFieldDec[Model](fieldName)

        if (field.isSuccess) {
            (model, fieldValue) => ReflectionHelper.getField[Model](field.get)(model).get.set(fieldValue)
        } else {
            val method = ReflectionHelper.getMethodDec[Model](fieldName + "_$eq")
            (model, fieldValue) => ReflectionHelper.getMethod(method.get)(model).get.apply(fieldValue)
        }
    }
}

final class MappedColumn[Model: ru.TypeTag: ClassTag, Field <: Any: ClassTag](colName: => String,
                                                                                   val fieldName: String)
                                                                (implicit c: ValueConverter[Field])
    extends Column[Model, Field, Field](colName) {
    def get(model: Model): SourceValue = c.toValue(getter(model))
    def set(model: Model, field: SourceValue): Unit = {
        val value = c.fromValue(field).getOrElse(field.value)

        try {
            setter(model, value.asInstanceOf[Field])
        } catch {
            case e: Throwable => Logger.getLogger(getClass.getName)
                .log(Level.SEVERE, s"Unable to set '$fieldName' to '$value'", e)
        }
    }
}

final class OptionalColumn[Model: ru.TypeTag: ClassTag, Field <: Any: ClassTag](colName: => String,
                                                                                     val fieldName: String)
                                                                              (implicit c: ValueConverter[Field])
    extends Column[Model, Option[Field], Field](colName) {
    def get(model: Model): Option[SourceValue] = {
        val mField = getter(model)

        if (mField.isDefined) Some(c.toValue(mField.get))
        else None
    }

    def set(model: Model, field: Option[SourceValue]): Unit = {
        val value = {
            if (field.isDefined) c.fromValue(field.get)
            else None
        }

        setter(model, value)
    }
}

sealed abstract class ForeignKeyBase[Model: ru.TypeTag: ClassTag, FModel: ru.TypeTag: ClassTag,
    Field: ru.TypeTag: ClassTag](colName: => String)
    extends Column[Model, Field, Field](colName) {
    private val fModelClass = implicitly[ClassTag[FModel]].runtimeClass

    val field: String
    val modelField: Option[String]
    val fkType: ClassTag[_]

    final def getProxyField(fModel: FModel)(implicit acc: IdAccessor) = Try({
        val mProxy = ReflectionHelper.getModelProxy[FModel](fModel)

        if (mProxy.isDefined) mProxy.get.field
        else acc.idOf(fModel).get
    })

    final def createProxy(modelId: SourceValue)(implicit acc: ModelAccessor): Try[FModel] = {
        val enhancer = new Enhancer()
        enhancer.setSuperclass(fModelClass)
        enhancer.setInterfaces(Array(classOf[Identifiable[_]]))
        enhancer.setCallbacks(Array(
            new ModelProxy[FModel](acc, modelId),
            NoOp.INSTANCE
        ))
        enhancer.setCallbackFilter(ReflectionHelper.FinalizeFilter)
        enhancer.setInterceptDuringConstruction(false)

        Try({
            val constructor = ReflectionHelper.getMinConstructor(fModelClass)
            val argTypes: Array[Class[_]] = constructor.getParameterTypes
            val args = ReflectionHelper.getDefaultArgs(constructor)

            enhancer.create(argTypes, args).asInstanceOf[FModel]
        })
    }
}

final class ForeignKey[Model: ru.TypeTag: ClassTag, FModel: ru.TypeTag: ClassTag](colName: => String,
                                                                                       val field: String,
                                                                                  val modelField: Option[String] = None,
                                                                                  val fkType: ClassTag[_])
    extends ForeignKeyBase[Model, FModel, FModel](colName) {
    private val fModelClass = implicitly[ClassTag[FModel]].runtimeClass

    val fieldName = modelField.getOrElse(field)

    def get(model: Model)(implicit acc: IdAccessor): SourceValue = {
        getProxyField(getter(model)).get
    }

    def set(model: Model, field: SourceValue)(implicit acc: ModelAccessor): Unit = {
        setter(model, createProxy(field).get)
    }
}

final class OptionalForeignKey[Model: ru.TypeTag: ClassTag, FModel: ru.TypeTag: ClassTag](colName: => String,
                                                                                               val field: String,
                                                                                  val modelField: Option[String] = None,
                                                                                  val fkType: ClassTag[_])
    extends ForeignKeyBase[Model, FModel, Option[FModel]](colName) {
    private val fModelClass = implicitly[ClassTag[FModel]].runtimeClass

    val fieldName = modelField.getOrElse(field)

    def get(model: Model)(implicit acc: IdAccessor): Option[SourceValue] = {
        val fModel = getter(model)

        if (fModel.isDefined) {
            val proxy = getProxyField(fModel.get)
            if (proxy.isSuccess) Some(proxy.get)
            else None
        } else None
    }

    def set(model: Model, field: Option[SourceValue])(implicit acc: ModelAccessor): Unit = {
        setter(model, {
            if (field.isDefined) {
                val proxy = createProxy(field.get)
                if (proxy.isSuccess) Some(proxy.get)
                else None
            }
            else None
        })
    }
}

sealed abstract class ForeignSetBase[Model: ru.TypeTag: ClassTag, FModel: ru.TypeTag: ClassTag](colName: => String)
    extends Column[Model, Set[FModel], Set[FModel]](colName) {
    val fModelTag = implicitly[ClassTag[FModel]]

    protected val foreignTable: TableDef[FModel, _]

    final def set(model: Model, id: SourceValue)(implicit db: Database[String], idAccessor: IdAccessor): Unit = {
        val enhancer = new Enhancer()
        enhancer.setSuperclass(classOf[Set[FModel]])
        enhancer.setCallbacks(Array(
            new ForeignSetProxy[FModel](getMatching(id, model).toSet),
            NoOp.INSTANCE
        ))
        enhancer.setCallbackFilter(ReflectionHelper.FinalizeFilter)

        setter(model, enhancer.create().asInstanceOf[Set[FModel]])
    }

    final def get(model: Model): Set[FModel] = getter(model)

    protected def getMatching(id: SourceValue, parent: Model)
                             (implicit db: Database[String], idAccessor: IdAccessor): Iterable[FModel]
}

final class ForeignSet[Model: ru.TypeTag: ClassTag, FModel: ru.TypeTag: ClassTag](colName: => String,
                                                                                       val fieldName: String,
                                                                                       val foreignTable: TableDef[FModel, _],
                                                          fkGetter: TableDef[FModel, _] => ForeignKey[FModel, Model])
    extends ForeignSetBase[Model, FModel](colName) {

    lazy val foreignKey = fkGetter(foreignTable)

    override protected def getMatching(id: SourceValue, parent: Model)
                                      (implicit db: Database[String], idAccessor: IdAccessor): Iterable[FModel] = {
        val found = db.get[FModel](Map(foreignTable.nameOf(foreignKey) -> id))

        found.foreach(fModel => {
            foreignKey.setter(fModel, parent)
        })

        found
    }
}

final class JoinedSet[Model: ru.TypeTag: ClassTag, FModel: ru.TypeTag: ClassTag, Join: ClassTag](colName: => String,
                                                                                                      val fieldName: String,
                                                                                 val joinTable: JoinTable[Join, _, Model, _, FModel, _])
    extends ForeignSetBase[Model, FModel](colName) {
    private val foreignClass = implicitly[ClassTag[FModel]].runtimeClass

    private def createForeign(enhancer: Enhancer): FModel = {
        lazy val constructor = ReflectionHelper.getMinConstructor(foreignClass)
        lazy val paramTypes = constructor.getParameterTypes
        lazy val args = ReflectionHelper.getDefaultArgs(constructor)

        enhancer.create(paramTypes, args).asInstanceOf[FModel]
    }

    override protected def getMatching(id: SourceValue, parent: Model)
                                      (implicit db: Database[String], idAccessor: IdAccessor): Iterable[FModel] = {
        lazy val queryA = Map(joinTable.nameOf(joinTable.parentId) -> id)
        def queryB(id: SourceValue): Map[String, SourceValue] = Map(joinTable.childTable.id -> id)

        val foundJoins = db.get[Join](queryA)

        val enhancer = new Enhancer()
        enhancer.setSuperclass(foreignClass)
        enhancer.setCallbackFilter(ReflectionHelper.FinalizeFilter)

        foundJoins.flatMap(j => {
            val fk = joinTable.childId.get(j)
            db.get[FModel](queryB(fk)).map(fModel => {
                enhancer.setCallbacks(Array(
                    new JoinedModelProxy(fModel, j.asInstanceOf[Join with Identifiable[_]]),
                    NoOp.INSTANCE
                ))
                createForeign(enhancer)
            })
        })
    }

    def createJoin(model: Model, fModel: Any): Try[Join] = Try({
        lazy val joinClass = implicitly[ClassTag[Join]].runtimeClass
        lazy val joinConstructor = ReflectionHelper.getMinConstructor(joinClass)
        lazy val defaultArgs = ReflectionHelper.getDefaultArgs(joinConstructor)

        val join = joinConstructor.newInstance(defaultArgs: _*).asInstanceOf[Join]
        joinTable.parentId.setter(join, model)
        joinTable.childId.setter(join, fModel.asInstanceOf[FModel])

        join
    })

    override protected val foreignTable: TableDef[FModel, _] = joinTable.childTable
}

object ReflectionHelper {
    def getCallback[T: ClassTag](obj: Any, index: Int = 0): Try[T] = {
        Try({
        val callbackMethod = obj.getClass.getMethod("getCallback", classOf[Int])
        callbackMethod.invoke(obj, int2Integer(index)).asInstanceOf[T]})
    }

    protected[source] def getModelProxy[T: ClassTag](obj: T): Option[ModelProxy[T]] = {
        val callback = Try(getCallback[ModelProxy[T]](obj).get)

        if (callback.isSuccess && callback.get.isInstanceOf[ModelProxy[T]])
            Some(callback.get)
        else None
    }

    protected[source] def getFieldDec[T: ru.TypeTag: ClassTag](fieldName: String): Try[ru.TermSymbol] = Try({
        ru.typeOf[T].decl(ru.TermName(fieldName)).asTerm.accessed.asTerm
    })

    protected[source] def getField[T: ru.TypeTag: ClassTag](fieldSymbol: ru.TermSymbol)(obj: T): Try[ru.FieldMirror] = Try({
        val modelMirror = ru.runtimeMirror(obj.getClass.getClassLoader)
        val im = modelMirror.reflect(obj)

        im.reflectField(fieldSymbol)
    })

    protected[source] def getMethodDec[T: ru.TypeTag: ClassTag](fieldName: String): Try[ru.MethodSymbol] = {
        try {
            Success(ru.typeOf[T].decl(ru.TermName(fieldName)).asMethod)
        } catch {
            case e: Throwable => Failure(e)
        }
    }

    protected[source] def getMethod[T: ru.TypeTag: ClassTag](methodSymbol: ru.MethodSymbol)(obj: T): Try[ru.MethodMirror] = {
        try {
            val modelMirror = ru.runtimeMirror(obj.getClass.getClassLoader)
            val im = modelMirror.reflect(obj)

            Success(im.reflectMethod(methodSymbol))
        } catch {
            case e: Throwable => Failure(e)
        }
    }

    def getMinConstructor(cls: Class[_]): Constructor[_] = {
        cls.getConstructors.minBy(_.getParameterCount)
    }

    def getDefaultArgs(constructor: Constructor[_]): Array[AnyRef] = {
        constructor.getParameterTypes.map((pClass: Class[_]) => pClass match {
            case x if x == classOf[String] => ""
            case x if x == classOf[Boolean] => false.asInstanceOf[AnyRef]
            case x if x == classOf[Int] ||
                x == classOf[Float] ||
                x == classOf[Short] ||
                x == classOf[Double] ||
                x == classOf[Long] ||
                x == classOf[Byte]  => 0.asInstanceOf[AnyRef]
            case x if x == classOf[Float] => 0.0f.asInstanceOf[AnyRef]
            case x if x == classOf[Option[_]] => None
            case _ => null
        })
    }

    object FinalizeFilter extends CallbackFilter {
        override def accept(method: Method): Int = method.getName match {
            case "finalize" => 1
            case _ => 0
        }
    }
}

protected[stORM] trait DirtyProxy[T] extends InvocationHandler {
    private var obj: Option[T] = None
    final def isDirty = obj.isDefined

    protected val loadOnce = true

    override def invoke(o: scala.Any, method: Method, args: Array[AnyRef]): AnyRef = {
        if (loadOnce && obj.isEmpty) obj = Some(loadObj())

        val mObj = {
            if (loadOnce) obj.get
            else loadObj()
        }

        method.invoke(mObj, args: _*)
    }

    protected def loadObj(): T
}

final private class ModelProxy[Model: ru.TypeTag: ClassTag](acc: ModelAccessor, val field: SourceValue)
    extends DirtyProxy[Model] {
    override protected def loadObj(): Model = acc.get[Model](field).get
}

final private class ForeignSetProxy[FModel](models: => Set[FModel]) extends DirtyProxy[Set[FModel]] {
    override protected def loadObj(): Set[FModel] = models

    override protected val loadOnce: Boolean = false
}

final class JoinedModelProxy[Join](_fModel: Any, _joinModel: Join with Identifiable[_]) extends DirtyProxy[Any] {
    override protected def loadObj(): Any = fModel

    def fModel = _fModel
    def joinModel = _joinModel

    override protected val loadOnce: Boolean = false
}