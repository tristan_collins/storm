package collins.tristan.stORM.source

import scala.reflect.ClassTag

trait DataSourceFactory[Key] {
    def newSource[Model: ClassTag]: DataSource[Key]
}
