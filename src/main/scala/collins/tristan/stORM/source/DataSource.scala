package collins.tristan.stORM.source

import collins.tristan.stORM.source.ReadableDataSource.{BinaryOperator, QueryModifier}

import scala.collection.immutable.Queue
import scala.concurrent.{Future, Promise}
import scala.reflect.ClassTag

final case class QueryParam[MapKey](column: MapKey,
                                    conditions: Queue[BinaryOperator],
                                    modifier: Option[QueryModifier] = None)

object QueryParam {
    def apply[MapKey](column: MapKey, conditions: BinaryOperator*): QueryParam[MapKey] =
        new QueryParam(column, Queue(conditions: _*))
}

object ReadableDataSource {
    abstract class BinaryOperator {
        def value: SourceValue

        final def apply(a: SourceValue, modifier: Option[QueryModifier] = None): Boolean = {
            if (modifier.isDefined) {
                val mod = modifier.get
                operate(mod(a), mod(value))
            } else operate(a, value)
        }

        private def operate(a: SourceValue, b: SourceValue): Boolean = {
            if (operate.isDefinedAt((a, b))) operate((a, b))
            else false
        }

        protected def operate: PartialFunction[(SourceValue, SourceValue), Boolean]
    }

    abstract class QueryModifier {
        final def apply(src: SourceValue): SourceValue = {
            if (modifier.isDefinedAt(src)) modifier(src)
            else src
        }

        protected def modifier: PartialFunction[SourceValue, SourceValue]
    }

    final case class LikeOperator(value: SourceValue) extends BinaryOperator {
        override protected def operate: PartialFunction[(SourceValue, SourceValue), Boolean] = {
            case (StringVal(a), StringVal(b)) => a.contains(b)
        }
    }

    final case class EqualsOperator(value: SourceValue) extends BinaryOperator {
        override protected def operate: PartialFunction[(SourceValue, SourceValue), Boolean] = {
            case (a, b) => a.value == b.value
        }
    }

    final case class GreaterThanOperator(value: SourceValue) extends BinaryOperator {
        override protected def operate: PartialFunction[(SourceValue, SourceValue), Boolean] = {
            case (IntVal(a), IntVal(b)) => a > b
        }
    }

    final case object LowercaseModifier extends QueryModifier {
        protected val modifier: PartialFunction[SourceValue, SourceValue] = (src: SourceValue) => src match {
            case StringVal(s) => s.toLowerCase
        }
    }

    final implicit def nonOpToQuery[MapKey](map: Map[MapKey, SourceValue]): Set[QueryParam[MapKey]] = {
        map.map{ case(key, value) =>
            QueryParam(key, EqualsOperator(value))
        }.toSet
    }
}

trait ReadableDataSource[MapKey] {
    def apply(dataMap: Map[MapKey, SourceValue]): Option[Map[MapKey, Option[SourceValue]]]
    def getAll(query: Set[QueryParam[MapKey]] = Set()): List[Map[MapKey, Option[SourceValue]]]

    final protected def matches(searchMap: Set[QueryParam[MapKey]])(m: Map[MapKey, Option[SourceValue]]): Boolean = {
        searchMap.takeWhile(param => {
            val opValue = m.getOrElse(param.column, None)

            if (opValue.isEmpty) false
            else {
                param.conditions
                    .takeWhile(op => op(opValue.get, param.modifier))
                    .size == param.conditions.size
            }
        }).size == searchMap.size
    }

    protected final def tryMatching[Row](rows: Iterable[Row],
                                         row2Map: Row => Map[MapKey, Option[SourceValue]],
                                         comp: Map[MapKey, Option[SourceValue]] => Boolean,
                                         rowChecker: Option[Row => Boolean] = None,
                                         groupSize: Int = 30) = {
        import scala.concurrent.ExecutionContext.Implicits.global
        val filtered = {
            if (rowChecker.isDefined) rows.filter(rowChecker.get)
            else rows
        }

        val futGroups = filtered.grouped(groupSize).map(group => {
            Future {
                group.map(row => {
                    val rowAsMap = row2Map(row)
                    if (comp(rowAsMap)) Some(rowAsMap)
                    else None
                }).flatten
            }
        })

        val p = Promise[List[Map[MapKey, Option[SourceValue]]]]

        (Future sequence futGroups) onSuccess {case groups =>
            p success groups.flatten.toList
        }

        p.future
    }
}

trait WritableDataSource[MapKey] {
    def put(dataMap: Map[MapKey, Option[SourceValue]]): Boolean
    def -=(dataMap: Map[MapKey, SourceValue]): List[Map[MapKey, Option[SourceValue]]]

    def insert: Insert[MapKey] = new Insert[MapKey](this)
}

trait DataSource[MapKey] extends ReadableDataSource[MapKey] with WritableDataSource[MapKey] {
    final type SourceType = this.type
    final type NonOpMap = Map[MapKey, SourceValue]
    final type DataMap = Map[MapKey, Option[SourceValue]]

    implicit def optional2Map(opMap: DataMap): NonOpMap =
        opMap.filterNot(_._2 == None).map(e => e._1 -> e._2.get)

    implicit def map2Optional(map: NonOpMap): DataMap =
        map.map(e => e._1 -> Some(e._2))

    implicit def map2Option(map: NonOpMap): Option[DataMap] = Some(map)
}

sealed abstract class SourceValue {
    def value: Any
}
case class IntVal(value: Int) extends SourceValue
case class BoolVal(value: Boolean) extends SourceValue
case class LongVal(value: Long) extends SourceValue
case class StringVal(value: String) extends SourceValue
case class ByteVal(value: Byte) extends SourceValue
case class DoubleVal(value: Double) extends SourceValue
case class FloatVal(value: Float) extends SourceValue

object SourceValue {
    implicit def int2Value(i: Int): IntVal = IntVal(i)
    implicit def bool2Value(b: Boolean): BoolVal = BoolVal(b)
    implicit def long2Value(l: Long): LongVal = LongVal(l)
    implicit def string2Value(s: String): StringVal = StringVal(s)
    implicit def byte2Value(b: Byte): ByteVal = ByteVal(b)
    implicit def double2Value(d: Double): DoubleVal = DoubleVal(d)
    implicit def float2Value(f: Float): FloatVal = FloatVal(f)

    implicit def parseValue[T: ClassTag](value: SourceValue): Option[T] = {
        import collins.tristan.stORM.converters.default._

        val tag = implicitly[ClassTag[T]]

        val parse = tag.runtimeClass match {
            case x if x == classOf[Int] => IntConverter.fromValue _
            case x if x == classOf[Boolean] => BooleanConverter.fromValue _
            case x if x == classOf[Long] => LongConverter.fromValue _
            case x if x == classOf[String] => StringConverter.fromValue _
            case x if x == classOf[Byte] => ByteConverter.fromValue _
            case x if x == classOf[Double] => DoubleConverter.fromValue _
            case x if x == classOf[Float] => FloatConverter.fromValue _
            case _ => v: SourceValue => None
        }

        val parsed = parse(value)
        if (parsed.isDefined) Some(parsed.get.asInstanceOf[T])
        else None
    }

    def convert(obj: Any): Option[SourceValue] = {
        if (obj == null) None
        else obj match {
            case x: Int => Some(x)
            case x: Boolean => Some(x)
            case x: Long => Some(x)
            case x: String => Some(x)
            case x: Byte => Some(x)
            case x: Double => Some(x)
            case x: Float => Some(x)
            case _ => Some(obj.toString)
        }
    }
}

final class Insert[MapKey](source: WritableDataSource[MapKey], toInsert: List[Map[MapKey, Option[SourceValue]]] = Nil)
    extends SourceTransaction[List[Map[MapKey, Option[SourceValue]]]] {
    final type DataMap = Map[MapKey, Option[SourceValue]]

    implicit def map2Optional(map: Map[MapKey, SourceValue]): DataMap =
        map.map(e => e._1 -> Some(e._2))

    def +(data: DataMap): Insert[MapKey] = {
        new Insert(source, toInsert :+ data)
    }

    def ++(data: Iterable[DataMap]): Insert[MapKey] = {
        new Insert(source, toInsert ++ data)
    }

    override def execute: List[DataMap] = {
        for(t <- toInsert;
            res = source.put(t)
            if res) yield t
    }
}

sealed abstract class SourceTransaction[Result] {
    def execute: Result
}