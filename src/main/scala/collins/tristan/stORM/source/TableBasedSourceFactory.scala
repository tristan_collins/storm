package collins.tristan.stORM.source

import scala.reflect.ClassTag

/**
 * Created by Tristan on 15-Apr-15.
 */
abstract class TableBasedSourceFactory[Key](tableAliases: Map[TableDef[_, _], String] = Map())
                                           (implicit protected val tableFor: ClassTag[_] => Option[TableDef[_, _]])
    extends DataSourceFactory[Key] {
    private val aliases = tableAliases.map(entry => entry._1.tableName -> entry._2)

    final override def newSource[Model: ClassTag]: DataSource[Key] = {
        val table = tableFor(implicitly[ClassTag[Model]])

        val tableName = {
            val cName = table.get.tableName
            aliases.getOrElse(cName, cName)
        }

        newSource[Model](tableName)
    }

    protected def newSource[Model: ClassTag](tableName: String): DataSource[Key]
}
