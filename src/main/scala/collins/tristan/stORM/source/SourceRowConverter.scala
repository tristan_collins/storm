package collins.tristan.stORM.source

import collins.tristan.stORM.dao.Identifiable

import scala.reflect.ClassTag

/*trait SourceRowConverter[Key, Model] {
    def model2Row(model: Model, idAccessor: IdAccessor): Map[Key, SourceValue]

    def row2Model(m: Map[Key, SourceValue]): Model
}*/

trait IdAccessor {
    def idOf[T: ClassTag](model: T): Option[SourceValue]
}

trait ModelAccessor {
    def get[T: ClassTag](id: SourceValue): Option[T with Identifiable[_]]
}