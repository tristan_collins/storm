package collins.tristan.stORM.observer

trait Observable[Args] {
    type RemMethod = () => Unit
    type Listener = (RemMethod, Args) => Unit

    private var listeners: List[Listener] = Nil

    private def rem(listener: Listener): () => Unit = () => { this -= listener }

    def +=(listener: Listener): RemMethod = {
        listeners = listeners :+ listener
        rem(listener)
    }

    def -=(listener: Listener): Unit = listeners = listeners.filterNot(_ == listener)

    def apply(args: Args): Unit = listeners.foreach(listener => {
        listener(rem(listener), args)
    })
}