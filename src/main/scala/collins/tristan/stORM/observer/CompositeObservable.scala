package collins.tristan.stORM.observer

import scala.collection.mutable

final class CompositeObservable[Id, Args] {
    private val listeners = new mutable.HashMap[Id, Observable[Args]]()

    def apply(id: Id): Observable[Args] = {
        if (listeners.contains(id)) listeners(id)
        else {
            val listener = new Observable[Args] { }
            listeners(id) = listener

            listener
        }
    }
}

object CompositeObservable {
    def apply[Id, Args] = new CompositeObservable[Id, Args]
}