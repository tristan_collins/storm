package collins.tristan.stORM.sources

import java.io._

import collins.tristan.stORM.source._

import scala.collection.immutable.Queue
import scala.concurrent._
import scala.concurrent.duration._
import scala.io.Source
import scala.reflect.ClassTag

trait DelimitedBasedSource extends DataSource[String] {
    import DelimitedBasedSource._

    protected def delimiter: String
    protected def columnOrder: Map[Column, Int]

    private val sortedColumns = columnOrder.toSeq.sortBy(_._2)
    private val sortedColumnsR = sortedColumns.map(entry => entry._2 -> entry._1).toMap

    final protected implicit def map2Row(map: DataMap): String = {
        sortedColumns.map(entry => {
            val srcValue: SourceValue = {
                val mapVal = map.get(entry._1.name)
                if (entry._1.optional && (mapVal.isEmpty || mapVal.get.isEmpty)) EMPTY_VALUE
                else if (mapVal.isDefined && mapVal.get.isDefined) mapVal.get.get
                else ""
            }

            escapeField(srcValue.value.toString)
        }).mkString(delimiter)
    }

    final protected implicit def row2Map(row: String): DataMap = {
        row.split(delimiter).zipWithIndex.map(entry => {
            val col = sortedColumnsR(entry._2)
            val strValue = entry._1.replaceAll(ESCAPE_DELIM, delimiter)

            val rawObj = col.valueTag.runtimeClass match {
                case x if x == classOf[Int] => strValue.toInt
                case x if x == classOf[Boolean] => strValue.toBoolean
                case x if x == classOf[Long] => strValue.toLong
                case x if x == classOf[Byte] => strValue.toByte
                case x if x == classOf[Double] => strValue.toDouble
                case x if x == classOf[Float] => strValue.toFloat
                case _ => deEscapeField(strValue)
            }

            if (col.optional && rawObj == EMPTY_VALUE) col.name -> None
            else col.name -> SourceValue.convert(rawObj)
        }).toMap
    }

    private def escapeField(field: String): String = {
        field.replace(delimiter, ESCAPE_DELIM)
    }

    private def deEscapeField(field: String): String = {
        field.replace(ESCAPE_DELIM, delimiter)
    }
}

object DelimitedBasedSource {
    private val ESCAPE_DELIM = "%delim%"
    private val EMPTY_VALUE = "%empty%"
}

final class DirectDelimitedSource(tableIn: => Source,
                                    tableOut: (Boolean) => OutputStream,
                                    val delimiter: String, val columnOrder: Map[Column, Int])
    extends DelimitedBasedSource {
    private def matches(query: String)(line: String): Boolean = {
        val querySplit = query.split(delimiter, -1)
        val lineSplit = line.split(delimiter, -1)

        if (querySplit.length > lineSplit.length) false
        else {
            val queryMap = querySplit.zipWithIndex.map(e => e._2 -> e._1).toMap
            val lineMap = lineSplit.zipWithIndex.map(e => e._2 -> e._1).toMap

            lineMap.takeWhile(e => {
                queryMap.contains(e._1) && {
                    val qVal = queryMap(e._1)
                    qVal.isEmpty || qVal == lineMap(e._1)
                }
            }).size == queryMap.size
        }
    }

    override def apply(query: Map[String, SourceValue]): Option[Map[String, Option[SourceValue]]] = {
        val queryString = map2Row(query)
        val result = tableIn.getLines().filter(LineChecker.isValid).find(matches(queryString))

        if (result.isDefined) Some(result.get)
        else None
    }

    override def getAll(query: Set[QueryParam[String]]): List[Map[String, Option[SourceValue]]] = {
        val lines = tableIn.getLines().toIterable

        val results = {
            if (query.isEmpty) lines.map(row2Map)
            else {
                val fut = tryMatching(lines, query)
                Await.result(fut, 2 minutes)
            }
        }

        results.toList
    }

    private def tryMatching(lines: Iterable[String], query: Set[QueryParam[String]]) = {
        tryMatching[String](lines, row2Map _, matches(query), Some(LineChecker.isValid _))
    }

    override def -=(query: Map[String, SourceValue]): List[Map[String, Option[SourceValue]]] = {
        val writer = new PrintWriter(tableOut(false))

        val queryString = map2Row(query)
        val (removed, remaining) = tableIn.getLines().partition(s => matches(queryString)(s))

        writer.write(remaining.mkString("\n"))
        writer.close()

        removed.map(row2Map).toList
    }

    override def put(dataMap: Map[String, Option[SourceValue]]): Boolean = {
        val writer = new PrintWriter(tableOut(true))
        writer.write(map2Row(dataMap))
        writer.close()

        true
    }
}

object DelimitedDataSource {
     protected[sources] def apply(table: File, delimiter: String, columnOrder: Map[Column, Int],
                                     cached: Boolean): DelimitedBasedSource = {
        def inputS: Source = {
            if (table.exists()) Source.fromFile(table)
            else Source.fromString("")
        }

        def outputS(append: Boolean = false): OutputStream = {
            if (append) {
                if (table.exists()) {
                    val writer = new PrintWriter(new FileOutputStream(table, append))
                    writer.write("\n")
                    writer.close()
                }
            } else if (table.exists()) table.delete()

            if (!table.exists()) table.createNewFile()

            new FileOutputStream(table, append)
        }

        if (cached) new CachedDelimitedSource(inputS, outputS, delimiter, columnOrder)
        else new DirectDelimitedSource(inputS, outputS, delimiter, columnOrder)
    }

    def apply(tableIn: Source, tableOut: (Boolean) => OutputStream, delimiter: String,
              colNames: Queue[String], tableDef: TableDef[_, _], cached: Boolean): DelimitedBasedSource = {
        val columnOrder = genColumnOrder(colNames, tableDef)
        if (cached) new CachedDelimitedSource(tableIn, tableOut, delimiter, columnOrder)
        else new DirectDelimitedSource(tableIn, tableOut, delimiter, columnOrder)
    }

    protected[sources] def columnTypes(table: TableDef[_, _]): Queue[(String, ClassTag[_], Boolean)] = {
        var tmpCols = Queue[(String, ClassTag[_], Boolean)]()
        tmpCols = tmpCols.enqueue((table.id, table.idType, false))
        tmpCols ++= table.columns.map(entry => (entry._1, entry._2.columnTag, false))
        tmpCols ++= table.foreignKeys.map(entry => (entry._1, entry._2.fkType, false))
        tmpCols ++= table.optionalColumns.map(entry => (entry._1, entry._2.columnTag, true))

        tmpCols
    }

    private def genColumnOrder(columnNames: Queue[String], table: TableDef[_, _]): Map[Column, Int] = {
        val colTypes: Map[String, (ClassTag[_], Boolean)] = columnTypes(table).map(entry => entry._1 -> (entry._2, entry._3)).toMap

        columnNames.zipWithIndex.map(x => {
            val (key, index) = x
            val (colType, optional) = colTypes(key)

            Column(key, colType, optional) -> index
        }).toMap
    }

    protected[sources] def genColumnOrder(types: Queue[(String, ClassTag[_], Boolean)]): Map[Column, Int] = {
        types.zipWithIndex.map({case (entry, ord) => {
            Column(entry._1, entry._2, entry._3) -> ord
        }}).toMap
    }
}

class DelimitedSourceFactory(path: File, delimiter: String, ext: String, cached: Boolean = true)
                                (implicit tableFor: ClassTag[_] => Option[TableDef[_, _]])
    extends TableBasedSourceFactory[String] {
    import DelimitedSourceFactory._

    override protected def newSource[Model: ClassTag](tableName: String): DataSource[String] = {
        val table = tableFor(implicitly[ClassTag[Model]]).get
        val cols = DelimitedDataSource.columnTypes(table)

        val colFile = new File(path, s"$tableName.$EXT_COL")
        val columnOrder: Map[Column, Int] = {
            if (colFile.exists) {
                val colMap: Map[String, (ClassTag[_], Boolean)] = cols.map(entry => entry._1 -> (entry._2, entry._3)).toMap
                val src = Source.fromFile(colFile)
                val res = src.getLines()
                    .filter(LineChecker.isValid)
                    .map(key => Column(key, colMap(key)._1, colMap(key)._2)).zipWithIndex.toMap
                src.close()
                res
            } else DelimitedDataSource.genColumnOrder(cols)
        }

        if (!colFile.exists) {
            if (!path.exists) path.mkdirs()

            colFile.createNewFile()
            val writer = new PrintWriter(colFile)
            writer.write(cols.map(_._1).mkString(System.lineSeparator()))
            writer.close()
        }

        DelimitedDataSource(new File(path, s"$tableName.$ext"), delimiter, columnOrder, cached)
    }
}

private object DelimitedSourceFactory {
    val EXT_COL = "col"
}

protected[sources] final case class Column(name: String, valueTag: ClassTag[_], optional: Boolean = false)

private object LineChecker {
    def isValid(line: String): Boolean = {
        val t = line.trim
        ! (t.isEmpty || t.startsWith("//") || t.startsWith("#"))
    }
}