package collins.tristan.stORM.sources

import java.io.{OutputStream, PrintWriter}

import collins.tristan.stORM.source.SourceValue

import scala.collection.mutable.ListBuffer
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.io.Source

/**
 * Created by Tristan on 24-Apr-15.
 */
final class CachedDelimitedSource(tableIn: => Source,
                                  tableOut: (Boolean) => OutputStream,
                                  val delimiter: String, val columnOrder: Map[Column, Int])
    extends MemoryBasedSource[String] with DelimitedBasedSource {
    private val mapFuture = Future {
        tableIn.getLines().map(row2Map)
    }

    override protected def loadMaps(): ListBuffer[Map[String, Option[SourceValue]]] = {
        val buff = ListBuffer[Map[String, Option[SourceValue]]]()

        buff ++= Await.result(mapFuture, 30 seconds)

        buff
    }

    override protected def onAdded(map: DataMap): Unit = {
        val writer = new PrintWriter(tableOut(true))
        writer.write(map2Row(map))
        writer.close()
    }

    override protected def onRemoved(map: List[DataMap]): Unit = {
        val writer = new PrintWriter(tableOut(false))
        writer.write(maps.map(map2Row).mkString("\n"))
        writer.close()
    }
}