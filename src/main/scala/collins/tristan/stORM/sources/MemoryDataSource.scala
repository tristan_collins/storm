package collins.tristan.stORM.sources

import collins.tristan.stORM.source.ReadableDataSource.nonOpToQuery
import collins.tristan.stORM.source.{DataSource, DataSourceFactory, QueryParam, SourceValue}

import scala.collection.mutable.ListBuffer
import scala.reflect.ClassTag

abstract class MemoryBasedSource[Key] extends DataSource[Key] {
    private lazy val modelMap = loadMaps()

    final override def getAll(query: Set[QueryParam[Key]]): List[Map[Key, Option[SourceValue]]] = {
        if (query.isEmpty) modelMap.toList
        else {
            modelMap.filter(matches(query)).toList
        }
    }

    final override def apply(searchMap: NonOpMap): Option[DataMap] = {
        modelMap.find(matches(searchMap))
    }

    final override def put(dataMap: DataMap): Boolean = {
        modelMap += dataMap
        onAdded(dataMap)

        true
    }

    final override def -=(searchMap: NonOpMap): List[DataMap] = {
        val result = modelMap.partition(matches(searchMap))

        modelMap --= result._1
        onRemoved(result._1.toList)

        result._1.toList
    }

    final protected def maps = modelMap.toList
    protected def loadMaps(): ListBuffer[Map[Key, Option[SourceValue]]]
    protected def onAdded(map: DataMap): Unit = {}
    protected def onRemoved(map: List[DataMap]): Unit = {}
}

final class MemoryDataSource[KeyType] extends MemoryBasedSource[KeyType] {
    override protected def loadMaps(): ListBuffer[DataMap] = ListBuffer()
}

class MemorySourceFactory[Key: ClassTag] extends DataSourceFactory[Key] {
    override def newSource[Model: ClassTag]: DataSource[Key] = {
        new MemoryDataSource[Key]
    }
}

object MemorySourceFactory {
    def apply[Key: ClassTag]: MemorySourceFactory[Key] = new MemorySourceFactory[Key]
}