package collins.tristan.stORM.sources

import java.sql.{Connection, DriverManager, ResultSet}

import collins.tristan.stORM.source.ReadableDataSource.{BinaryOperator, EqualsOperator, LikeOperator, LowercaseModifier}
import collins.tristan.stORM.source._
import collins.tristan.stORM.sources.JdbcDataSource.{EmptyLogin, Login}

import scala.collection.immutable.Queue
import scala.collection.mutable.ListBuffer

final class JdbcUrl(subProtocol: String, host: String, database: String, port: Option[Int]) {
    override def toString: String = {
        s"jdbc:$subProtocol://$host${if (port.isDefined) ":" + port.get else ""}/$database"
    }
}

object JdbcUrl {
    implicit def toString(url: JdbcUrl): String = url.toString

    def apply(subProtocol: String, host: String, database: String, port: Int): JdbcUrl =
        new JdbcUrl(subProtocol, host, database, Some(port))

    def apply(subProtocol: String, host: String, database: String): JdbcUrl =
        new JdbcUrl(subProtocol, host, database, None)
}

final class JdbcDataSource(driver: String, url: JdbcUrl, table: String, login: Login = EmptyLogin)
    extends DataSource[String] {
    Class.forName(driver)

    private def connect() = {
        if (login.username.isDefined && login.password.isDefined)
            DriverManager.getConnection(url, login.username.get, login.password.get)
        else if (login.username.isDefined && login.password.isEmpty)
            DriverManager.getConnection(url, login.username.get, "")
        else DriverManager.getConnection(url)
    }

    private def toSQL(map: NonOpMap) = {
        map.map(entry => {
            s"${entry._1}=${entry._2: String}"
        }).mkString(",")
    }

    implicit private def valueToString(value: SourceValue): String = value match {
        case StringVal(str) => s"'$str'"
        case v => v.value.toString
    }

    private def queryAsSql(query: Set[QueryParam[String]], columns: Option[Set[String]] = None): String = {
        def getVal(param: QueryParam[String], value: SourceValue): String = {
            (if (param.modifier.isDefined) param.modifier.get.apply(value)
            else value): String
        }

        def getConditionColumn(param: QueryParam[String]): String = {
            if (param.modifier.isEmpty) param.column
            else {
                param.modifier.get match {
                    case LowercaseModifier => s"LOWER(${param.column})"
                    case _ => param.column
                }
            }
        }

        val colString = {
            if (columns.isDefined) columns.get.mkString(",")
            else "*"
        }

        val conditions = query.map(param => param.conditions.collect {
            case EqualsOperator(v) => Some(s"${getConditionColumn(param)}=${getVal(param, v)}")
            case LikeOperator(v) => Some(s"${getConditionColumn(param)} LIKE ${getVal(param, "%" + v.value.toString + "%")}")
            case _ => None
        }.flatten).flatten

        if (conditions.isEmpty) s"SELECT $colString FROM $table"
        else {
            s"SELECT $colString FROM $table WHERE ${conditions.mkString(",")};"
        }
    }

    private implicit def nonOpToQuery(map: NonOpMap): Set[QueryParam[String]] = {
        map.map { case (column, value) =>
            QueryParam(column, Queue(EqualsOperator(value)))
        }.toSet
    }

    private def queryResults(conn: Connection, query: Set[QueryParam[String]] = Set(),
                             columns: Option[Set[String]] = None): ResultSet = {
        conn.createStatement().executeQuery(queryAsSql(query, columns))
    }

    private def parseRow(result: ResultSet): Option[DataMap] = {
        if (!result.isAfterLast) {
            val meta = result.getMetaData
            val columns = (1 to meta.getColumnCount).map(meta.getColumnName)
            val readable = columns.map(col => {
                try {
                    col -> result.getObject(col)
                } catch {
                    case e: Throwable => col -> null
                }
            }).map(entry => {
                entry._1 -> {
                    if (entry._2 == null) None
                    else Some(SourceValue.convert(entry._2).get)
                }
            })

            Some(readable.toMap)
        } else None
    }

    override def apply(query: NonOpMap): Option[DataMap] = {
        val conn = connect()
        val result = queryResults(conn, query)
        val returnVal = if (result.first()) parseRow(result) else None

        conn.close()
        returnVal
    }

    override def -=(dataMap: NonOpMap): List[DataMap] = {
        val conn = connect()
        val matching = getAll(dataMap)

        val delCount = conn.createStatement().executeUpdate(s"DELETE FROM $table WHERE ${toSQL(dataMap)}")

        conn.close()

        if (delCount >= 0) matching
        else List()
    }

    override def put(dataMap: Map[String, Option[SourceValue]]): Boolean = {
        val conn = connect()
        val s = conn.createStatement()

        val result = s.execute({
            val strValues = dataMap.values.map(s => {
                if (s.isEmpty) "NULL"
                else s.get match {
                    case StringVal(str) => s"'$str'"
                    case v => v.value.toString
                }
            })

            s"INSERT INTO $table (${dataMap.keys.mkString(",")}) VALUES (${strValues.mkString(",")})"
        })

        conn.close()
        result
    }

    private def parseResultSet(result: ResultSet): List[DataMap] = {
        val matching = {
            if (!result.first()) List()
            else {
                val buff = ListBuffer[DataMap]()

                while (!result.isAfterLast) {
                    val m = parseRow(result)
                    if (m.isDefined) buff += m.get

                    result.next()
                }

                buff.toList
            }
        }

        matching
    }

    override def getAll(query: Set[QueryParam[String]]): List[Map[String, Option[SourceValue]]] = {
        val conn = connect()
        val result = queryResults(conn, query)
        val found = parseResultSet(result)

        val postQueryOps = query.map(_.conditions).flatten
            .filter(op => JdbcDataSource.supportedOperators.contains(op.getClass))

        val results = {
            if (postQueryOps.isEmpty) found
            else found.filter(matches(query))
        }

        conn.close()

        results
    }
}

object JdbcDataSource {
    protected[sources] val supportedOperators =
        Set[Class[_ <: BinaryOperator]](classOf[EqualsOperator], classOf[LikeOperator])

    sealed class Login(val username: Option[String], val password: Option[String])
    object EmptyLogin extends Login(None, None)
    final class UserLogin(login: (String, String)) extends Login(Some(login._1), Some(login._2))

    object Login {
        implicit def toLogin(login: (String, String)): Login = new UserLogin(login)
        implicit def toLogin(username: String): Login = new UserLogin((username, ""))
    }

    def apply(driver: String, url: JdbcUrl, table: String, login: Login = EmptyLogin): JdbcDataSource =
        new JdbcDataSource(driver, url, table, login)
}