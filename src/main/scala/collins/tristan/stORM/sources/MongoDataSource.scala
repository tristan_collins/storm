package collins.tristan.stORM.sources

import collins.tristan.stORM.source._
import com.mongodb.casbah.commons.MongoDBObject
import com.mongodb.casbah.{Imports, MongoClient, MongoCollection}

import scala.collection.JavaConversions
import scala.reflect.ClassTag

/**
 * Created by Tristan on 15-Apr-15.
 */
class MongoDataSource(table: MongoCollection) extends DataSource[String] {
    override def apply(dataMap: NonOpMap): Option[DataMap] = {
        val found = table.findOne(dataMap: DataMap)

        if (found.isDefined) Some(found.get)
        else None
    }

    override def -=(dataMap: NonOpMap): List[DataMap] = {
        val found = table.find(dataMap: DataMap)
        table.remove(dataMap: DataMap)

        found.toList.map(obj2Map)
    }

    override def put(dataMap: DataMap): Boolean = {
        table.insert(dataMap)
        true
    }

    implicit private def obj2Map(obj: Imports.DBObject): DataMap = {
        import JavaConversions._
        obj.keySet.map(key => key -> SourceValue.convert(obj.get(key))).toMap
    }

    implicit private def map2Obj(m: DataMap): Imports.DBObject = {
        val builder = MongoDBObject.newBuilder

        m.foreach(entry => {
            builder += entry._1 -> {
                if (entry._2.isDefined) entry._2.get.value else null
            }
        })

        builder.result()
    }

    override def getAll(query: Set[QueryParam[String]]): List[Map[String, Option[SourceValue]]] = {
        val result = {
            if (query.isEmpty) table.find().map(obj2Map)
            else {
                table.find().map(obj2Map).filter(m => matches(query)(m))
            }
        }

        result.toList
    }
}

class MongoSourceFactory(dbName: String, host: String = "localhost", port: Int = 27017)
                        (implicit tableFor: ClassTag[_] => Option[TableDef[_, _]])
    extends TableBasedSourceFactory[String] {
    private lazy val client = MongoClient(host, port)
    private lazy val db = client(dbName)

    override def newSource[Model: ClassTag](tableName: String): DataSource[String] = {
        new MongoDataSource(db(tableName))
    }
}
