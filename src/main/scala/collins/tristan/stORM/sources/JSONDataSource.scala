package collins.tristan.stORM.sources

import java.io.{File, PrintWriter}
import java.util.logging.{Level, Logger}

import collins.tristan.stORM.source.{DataSource, SourceValue, _}
import collins.tristan.stORM.sources.JSONDataSource._
import org.json._

import scala.collection.JavaConversions._
import scala.collection.mutable.ListBuffer
import scala.io.Source
import scala.reflect.ClassTag

class JSONDataSource(file: File) extends MemoryBasedSource[String] {
    private var bufCounter = 0

    override protected def loadMaps(): ListBuffer[DataMap] = {
        if (file.exists()) {
            val src = Source.fromFile(file)

            try {
                val jsonArray = new JSONObject(src.getLines().mkString).getJSONArray(KEY_ROOT)
                ListBuffer() ++ (0 until jsonArray.length).map(jsonArray.getJSONObject).map(mapFromJSON).toList
            } catch {
                case e: Exception =>
                    Logger.getLogger(getClass.getName).log(Level.SEVERE, null, e)
                    ListBuffer()
            } finally {
                src.close()
            }
        } else ListBuffer()
    }

    private def mapFromJSON(obj: JSONObject): DataMap = {
        obj.keySet()
            .map(key => key -> obj.get(key))
            .map({case (key, value) =>
                if (value == null) key -> None
                else key -> Some(SourceValue.convert(value).get)
            }).toMap
    }

    private def jsonFromMap(map: DataMap): JSONObject = {
        val jObj = new JSONObject()
        map.foreach({case (key, value) =>
            if (value.isEmpty) jObj.put(key, JSONObject.NULL)
            else jObj.put(key, value.get.value)
        })

        jObj
    }

    override protected def onAdded(map: DataMap): Unit = onAltered(maps, 1)
    override protected def onRemoved(map: List[DataMap]): Unit = onAltered(maps, map.length)

    private def onAltered(maps: List[DataMap], count: Int): Unit = {
        bufCounter += count
        if (bufCounter >= BUFFER_SIZE) {
            save(maps)
            bufCounter = 0
        }
    }

    private def save(maps: List[DataMap]): Unit = {
        val root = new JSONObject
        val rootArray = new JSONArray

        maps.foreach(m => rootArray.put(jsonFromMap(m)))
        root.put(KEY_ROOT, rootArray)

        if (file.exists()) file.delete()

        val writer = new PrintWriter(file)
        writer.write(root.toString)
        writer.close()
    }

    override def finalize(): Unit = {
        save(maps)
        super.finalize()
    }
}

private object JSONDataSource {
    val KEY_ROOT = "models"
    val BUFFER_SIZE = 1
}

class JSONSourceFactory(private val path: String, private val ext: String)
                       (implicit tableFor: ClassTag[_] => Option[TableDef[_, _]])
    extends TableBasedSourceFactory[String] {
    override def newSource[Model: ClassTag](tableName: String): DataSource[String] = {
        val fileName = tableName + "." + ext
        new JSONDataSource(new File(path, fileName))
    }
}

object JSONSourceFactory {
    def apply(path: String = "data", ext: String = "json")
             (implicit tableFor: ClassTag[_] => Option[TableDef[_, _]]): JSONSourceFactory =
        new JSONSourceFactory(path, ext)
}