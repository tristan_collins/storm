package collins.tristan.stORM.sources.jdbc

import collins.tristan.stORM.source._
import collins.tristan.stORM.sources.{JdbcDataSource, JdbcUrl}

import scala.reflect.ClassTag

class MySQLSourceFactory(protected val database: String, protected val username: String, protected val password: String,
                         protected val host: String = "localhost", protected val port: Int = 3306,
                            tableAliases: Map[TableDef[_, _], String] = Map())
                        (implicit tableFor: ClassTag[_] => Option[TableDef[_,_]])
    extends TableBasedSourceFactory[String](tableAliases) {
    final override def newSource[Model: ClassTag](tableName: String): DataSource[String] = {
        JdbcDataSource("com.mysql.jdbc.Driver", JdbcUrl("mysql", host, database, port), tableName, (username, password))
    }
}

object MySQLSourceFactory {
    def apply(database: String, username: String, password: String,
              host: String = "localhost", port: Int = 3306)
             (implicit tableFor: ClassTag[_] => Option[TableDef[_, _]]) = {
        new MySQLSourceFactory(database, username, password, host, port)
    }
}