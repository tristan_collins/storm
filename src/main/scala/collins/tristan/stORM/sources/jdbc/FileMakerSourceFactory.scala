package collins.tristan.stORM.sources.jdbc

import collins.tristan.stORM.source.{DataSource, TableBasedSourceFactory, TableDef}
import collins.tristan.stORM.sources.{JdbcDataSource, JdbcUrl}

import scala.reflect.ClassTag

final class FileMakerSourceFactory(database: String, username: String, password: String, host: String,
                                   tableAliases: Map[TableDef[_, _], String] = Map())
                        (implicit tableFor: ClassTag[_] => Option[TableDef[_,_]])
    extends TableBasedSourceFactory[String](tableAliases) {
    final override def newSource[Model: ClassTag](tableName: String): DataSource[String] = {
        JdbcDataSource("com.filemaker.jdbc.Driver", JdbcUrl("filemaker", host, database), tableName, (username, password))
    }
}

object FileMakerSourceFactory {
    def apply(database: String, username: String, password: String = "", host: String = "localhost")
             (implicit tableFor: ClassTag[_] => Option[TableDef[_, _]]) = {
        new FileMakerSourceFactory(database, username, password, host)
    }
}