package collins.tristan.stORM.dao

import collins.tristan.stORM.source._

import scala.reflect.ClassTag

abstract class SimpleDaoFactory[Id: ClassTag]
    extends MixedDaoFactory {
    protected final def register[Model: ClassTag](table: TableDef[Model, Id]): Unit = register[Model, Id](table)

    protected def newIdGen[Model: ClassTag]: IdGenerator[Id]
    protected val sourceFactory: DataSourceFactory[String]

    final override protected def newIdGen[Id: ClassTag, Model: ClassTag]: IdGenerator[Id] = {
        newIdGen[Model].asInstanceOf[IdGenerator[Id]]
    }
}