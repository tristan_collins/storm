package collins.tristan.stORM.dao

import collins.tristan.stORM.source.ReadableDataSource.{EqualsOperator, GreaterThanOperator, LikeOperator, LowercaseModifier}
import collins.tristan.stORM.source._

import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future, Promise}
import scala.reflect.ClassTag

class ModelDao[IdType: ClassTag, ModelType: ClassTag]
    (private val idGen: () => IdType,
        protected val dataSource: DataSource[String],
        private val modelConverter: ModelConverter[IdType, ModelType]) extends Dao[IdType, ModelType] {
    import dataSource.{map2Optional, optional2Map}
    import modelConverter.{id2Value, idKey}

    val modelTag = implicitly[ClassTag[ModelType]]

    private def model2Map(id: IdType, model: ModelType): dataSource.DataMap = modelConverter.model2Map(this, id, model)

    private implicit def map2Model(map: dataSource.DataMap): IdModel = {
        val model = modelConverter.map2Model(this, map)
        assignId(model.id, model)
        model
    }

    private val modelRefMap = new mutable.HashMap[Int, IdType]

    private def fireEvents(f: (IdModel) => Unit)(models: Iterable[IdModel]) = models.foreach(m => f(m))
    protected final def fireAdd(models: Iterable[IdModel]) = {
        fireEvents(OnModelAdded(this, _))(models)
    }
    protected final def fireRemoveStart(model: IdModel) = {
        fireEvents(OnModelRemoveStarted(this, _))(List(model))
    }
    protected final def fireRemove(models: List[IdModel]) = {
        fireEvents(OnModelRemoved(this, _))(models)

        models.foreach(m => RemovedComposite(m.id)(this, m))
    }
    protected final def fireUpdate(models: Iterable[IdModel]) = {
        fireEvents(OnModelUpdated(this, _))(models)

        models.foreach(m => UpdatedComposite(idOf(m).get)(this, m))
    }

    private def assignId(id: IdType, model: ModelType): IdModel = model match {
        case x: Identifiable[IdType] if x.id == id => model.asInstanceOf[IdModel]
        case _ => {
            modelRefMap(System.identityHashCode(model)) = id
            modelConverter.wrapModel(id, model)
        }
    }
    private def unassignId(model: ModelType): Unit = {
        val hashCode = System.identityHashCode(model)

        if (modelRefMap.contains(hashCode)) modelRefMap -= hashCode
    }

    final def apply(id: IdType): Option[IdModel] = {
        val idValue: SourceValue = id
        val m = dataSource(Map(idKey -> idValue))

        if (m.isDefined) {
            val model: IdModel = m.get
            Some(assignId(id, model))
        } else None
    }

    final def get(model: ModelType): Option[ModelType] = {
        val id = idOf(model)
        if (id.isDefined) apply(id.get)
        else None
    }

    final def update(model: IdModel): Option[IdModel] =
        update(model.id, model)

    final def update(id: IdType, model: ModelType): Option[IdModel] =
        update(id, model, false)

    final def update(id: IdType, model: ModelType, ifChanged: Boolean): Option[IdModel] = {
        def compMap[A, B](m1: dataSource.DataMap, m2: dataSource.DataMap): Boolean = {
            if (m1.size != m2.size) false
            else {
                m1.takeWhile(entry => {
                    m2.contains(entry._1) && {
                        val (valA, valB) = (m2(entry._1), entry._2)
                        valA.isDefined == valB.isDefined &&
                        valA.get.value == valB.get.value
                    }
                }).size == m2.size
            }
        }

        val idQuery: dataSource.DataMap = Map(idKey -> Some(id))
        val origMap = dataSource(idQuery)

        val returnModel = assignId(id, model)
        val map = model2Map(id, model) ++ idQuery

        val hasChanged = if (origMap.nonEmpty) !compMap(origMap.get, map) else true
        val canUpdate = !ifChanged || hasChanged

        if (canUpdate) {
            if (hasChanged) {
                if (origMap.nonEmpty) dataSource -= origMap.get
                dataSource.put(map)
            }

            /*val returnModel: IdModel = model match {
                case x: Identifiable[IdType] => {
                    if (x.id == id) model.asInstanceOf[IdModel]
                    else map
                }
                case _ => map
            }*/

            if (origMap.nonEmpty) fireUpdate(List(returnModel))
            else fireAdd(List(returnModel))

            Some(returnModel)
        }
        else None
    }

    final def update(model: ModelType): Boolean = {
        val retModel = model match {
            case x: IdModel =>
                dataSource.put(model2Map(x.id, x))
                x
            case _ =>
                val m = model2Map(idOf(model).get, model)
                dataSource.put(m)
                map2Model(m)
        }

        fireUpdate(List(retModel))
        true
    }

    final def idOf(model: ModelType): Option[IdType] = model match {
        case x: Identifiable[IdType] => Some(x.id)
        case _ => {
            val hashCode = System.identityHashCode(model)

            if (modelRefMap.contains(hashCode)) Some(modelRefMap(hashCode))
            else None
        }
    }

    final def srcId(model: ModelType): Option[SourceValue] = {
        val id = idOf(model)

        if (id.isDefined) Some(id.get)
        else None
    }

    final private def convertMaps(maps: Iterable[dataSource.DataMap]): List[IdModel] = {
        val p = Promise[List[IdModel]]

        val groupSize = (maps.size / 4) max 1
        val groups = maps.grouped(groupSize)
        val conGroups = Future sequence groups.map(g => Future { g.map(map2Model) })

        conGroups onSuccess {
            case models => p success models.flatten.toList
        }

        Await.result(p.future, 2 minute)
    }

    final def getAll: List[IdModel] = {
        convertMaps(dataSource.getAll())
    }

    final def find(query: Map[String, SourceValue]): List[IdModel] =
        find(ReadableDataSource.nonOpToQuery[String](query))

    final def find(query: QueryParam[String]*): List[IdModel] = find(query.toSet)

    final def find(query: Set[QueryParam[String]]): List[IdModel] = {
        convertMaps(dataSource.getAll(query))
    }

    /**
     * Called to find a matching model.
     * A model is only returned if a single result was found.
     *
     * @param model Model to search for
     * @return Optional model
     */
    protected[stORM] final def find(model: ModelType): Option[IdModel] = {
        val res = dataSource.getAll(ReadableDataSource.nonOpToQuery(modelConverter.model2Map(model)))
        if (res.length == 1) Some(map2Model(res.head))
        else None
    }

    final private def add(data: Iterable[ModelType]): List[IdModel] = {
        var insert = dataSource.insert
        val models = data.map(p => {
            val id = idGen()
            val map: dataSource.DataMap = model2Map(id, p)

            insert = insert + (map + ((idKey, Some(id))))

            assignId(id, p)
        })

        val result = insert.execute

        fireAdd(models)
        models.toList
    }

    final protected def remove(dataMap: dataSource.DataMap): List[IdModel] = {
        remove(dataMap, true)
    }

    private def remove(dataMap: dataSource.DataMap, fireEvent: Boolean): List[IdModel] = {
        val found = dataSource(dataMap)
        val removed = found.map(map => map2Model(map) -> map).map(x => (x._1, x._2))
        removed.foreach(entry => {
            val model = entry._1
            val map = entry._2

            fireRemoveStart(model)
            dataSource -= map
            unassignId(model)
        })

        val removedModels = removed.map(_._1).toList
        if (fireEvent) fireRemove(removedModels)

        removedModels
    }

    final def remove(data: Iterable[ModelType]): List[IdModel] = {
        var removed = List[IdModel]()

        for (m <- data; id = idOf(m); if id.isDefined) {
            removed = removed :+ remove(Map(idKey -> Some(id.get)), false).head
        }

        fireRemove(removed)
        removed
    }

    final def +=(model: ModelType): IdModel = add(List(model)).head

    final def +=(model: ModelType, models: ModelType*): List[IdModel] = add(model +: models)

    final def +=(data: Iterable[ModelType]): List[IdModel] = add(data)

    final def -=(data: ModelType*): List[IdModel] = remove(data)

    final def -=(data: Iterable[ModelType]): List[IdModel] = remove(data)

    final def -=(query: dataSource.NonOpMap): List[IdModel] = remove(query)
}

object ModelDao {
    implicit class ModifiedParam(param: QueryParam[String]) {
        def as_lowercase: QueryParam[String] = QueryParam(param.column, param.conditions, Some(LowercaseModifier))
    }

    implicit class RichColumn[Model, Field, _](col: Column[Model, Field, _]) {
        def EQUALS(value: Field): QueryParam[String] = EQUALS(SourceValue.convert(value).get)

        def EQUALS(value: SourceValue): QueryParam[String] = {
            QueryParam(col.name, EqualsOperator(value))
        }

        def :==(value: Field) = EQUALS(value)
    }

    implicit class RichStringColumn[Model, Field](col: Column[Model, Field, String]) extends RichColumn(col) {
        def LIKE(value: String): QueryParam[String] = {
            QueryParam(col.name, LikeOperator(SourceValue.convert(value).get))
        }
    }

    implicit class RichIntColumn[Model, Field](col: Column[Model, Field, Int]) extends RichColumn(col) {
        def :>(value: Int) = QueryParam(col.name, GreaterThanOperator(value))
    }
}