package collins.tristan.stORM.dao

import java.util.logging.{Level, Logger}

import collins.tristan.stORM.source._

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.reflect.ClassTag
import scala.util.Success

abstract class MixedDaoFactory
    extends DaoFactory[String] {
    private val tables = mutable.HashMap[Class[_], TableDef[_, _]]()
    private val tableIds = mutable.HashMap[Class[_], ClassTag[_]]()
    private val tableDaos = mutable.HashMap[TableDef[_, _], ModelDao[Any, Any]]()

    private val waitingList = mutable.HashMap[TableDef[_, _], ListBuffer[TableDef[_, _]]]()

    final def get[Model: ClassTag]: Option[ModelDao[Any, Model]] = {
        val modelTag = implicitly[ClassTag[Model]]
        val modelClass = modelTag.runtimeClass
        val idTag = tableIds(modelClass)

        val dao = get(idTag, modelTag)
        if (dao.isDefined) Some(dao.get.asInstanceOf[ModelDao[Any, Model]])
        else None
    }

    override protected final def newDao[Id: ClassTag, Model: ClassTag](source: DataSource[String],
                                                                       idSource: ModelIdAccessor,
                                                                       idGenerator: IdGenerator[Id]): Option[ModelDao[Id, _]] = {
        val modelClass = implicitly[ClassTag[Model]].runtimeClass

        if (tables.contains(modelClass)) {
            Some(new ModelDao[Id, Model](
                idGenerator.gen _,
                source,
                newConverter[Id, Model](idSource, tables(modelClass).asInstanceOf[TableDef[Model, Id]], idGenerator)))
        } else None
    }

    protected final def register[Model: ClassTag, Id: ClassTag](table: TableDef[Model, Id]): Unit = {
        val modelTag = implicitly[ClassTag[Model]]
        val idTag = implicitly[ClassTag[Id]]

        tables(modelTag.runtimeClass) = table
        tableIds(modelTag.runtimeClass) = idTag

        val dao = get[Id, Model].get
        dao.OnModelAdded += execForeignSet(handleDaoChange)
        dao.OnModelUpdated += execForeignSet(handleDaoChange)
        dao.OnModelRemoveStarted += execForeignSet(handleModelRemoval, false)

        tableDaos(table) = dao.asInstanceOf[ModelDao[Any, Any]]
        val tableDao = tableDaos(table)

        if (table.tableParent.isDefined) {
            val parent = table.tableParent.get

            if (tables.values.exists(_ == parent)) {
                listenToChild(tableDaos(parent), tableDao)
            } else addToWaiting[Model](parent, table)
        }

        if (waitingList.contains(table)) {
            val waitingTables = waitingList(table).toList
            waitingList(table).clear()

            waitingTables.foreach(w => {
                listenToChild(tableDao, tableDaos(w))
            })
        }
    }

    private def addToWaiting[Child](parentDef: TableDef[_ >: Child, _], childDef: TableDef[Child, _]): Unit = {
        if (!waitingList.contains(parentDef)) waitingList(parentDef) = new ListBuffer()

        waitingList(parentDef) += childDef
    }

    private def listenToChild(parentDao: ModelDao[Any, Any], childDao: ModelDao[Any, Any]) = {
        childDao.OnModelAdded += {case (stop, args) =>
            parentDao(args._2.id) = args._2
        }

        childDao.OnModelRemoved += {case (stop, args) =>
            parentDao -= args._2
        }

        childDao.OnModelUpdated += {case (stop, args) =>
            parentDao(args._2.id) = args._2
        }
    }

    private def execForeignSet[Id, Model](f: (Any, ModelDao[Any, Any]) => Any with Identifiable[_], onlyIfDirty: Boolean = true)
                                         (stop: () => Unit, args: (Dao[Id, Model], Model with Identifiable[Id])): Unit = {
        val (model, table) = (args._2, tables(args._1.asInstanceOf[ModelDao[Id, Model]].modelTag.runtimeClass))
        table.foreignSets.foreach(entry => {
            val fSet = entry._2.asInstanceOf[ForeignSetBase[Model, AnyRef]]
            val fDao = get(fSet.fModelTag).get.asInstanceOf[ModelDao[Any, Any]]

            val fModels = fSet.get(model)
            val callback = ReflectionHelper.getCallback[DirtyProxy[Any]](fModels)

            val isDirty = try {
                callback.get.isDirty
            } catch {
                case _: Throwable => true
            }

            if (isDirty || !onlyIfDirty) {
                fSet match {
                    case fSet: ForeignSet[Model, _] =>
                        fModels.filter(m => {
                            val callback = ReflectionHelper.getCallback[DirtyProxy[Model]](m)
                            if (callback.isSuccess) callback.get.isDirty
                            else true
                        }).foreach(f(_, fDao))
                    case fSet: JoinedSet[Model, _, _] =>
                        val jDao = get(fSet.joinTable.modelType).get.asInstanceOf[ModelDao[Any, Any]]
                        val joinTable = fSet.joinTable

                        fModels.map(m => {
                            val callback = ReflectionHelper.getCallback[JoinedModelProxy[_]](m)
                            if (callback.isSuccess) f(callback.get.fModel, fDao)
                            else f(m, fDao)
                        }).map(m => {
                            val callback = ReflectionHelper.getCallback[JoinedModelProxy[_]](m)

                            if (callback.isSuccess && callback.get.isInstanceOf[JoinedModelProxy[_]])
                                Some(callback.get.joinModel)
                            else {
                                val j = {
                                    val found = jDao.find(Map(
                                        joinTable.nameOf(joinTable.parentId) -> model.srcId,
                                        joinTable.nameOf(joinTable.childId) -> m.srcId))

                                    if (found.nonEmpty) Success(found.head)
                                    else fSet.createJoin(model, m)
                                }
                                if (j.isSuccess) Some(j.get)
                                else {
                                    Logger.getLogger(this.getClass.getName)
                                        .log(Level.SEVERE, "Error creating join model", j.failed.get)
                                    None
                                }
                            }
                        }).filterNot(_ == None).foreach(m => f(m.get, jDao))
                    case _ =>
                }
            }
        })
    }

    private def handleDaoChange(m: Any, fDao: ModelDao[Any, Any]): Any with Identifiable[Any] = {
        val fId = fDao.idOf(m)

        if (fId.isEmpty) fDao += m
        else fDao.update(fId.get, m, true).getOrElse(m.asInstanceOf[Any with Identifiable[Any]])
    }

    private def handleModelRemoval(m: Any, fDao: ModelDao[Any, Any]): Any with Identifiable[_] = {
        fDao.remove(List(m)).head
    }

    private def newConverter[Id: ClassTag, Model: ClassTag](_idSource: ModelIdAccessor, _table: TableDef[Model, Id],
                                                               idGenerator: IdGenerator[Id]) = {
        new ModelConverter[Id, Model] {
            override protected def idAccessor: ModelIdAccessor = _idSource

            override protected def idConverter: ValueConverter[Id] = {
                new ValueConverter[Id] {
                    override implicit def toValue(from: Id): SourceValue = idGenerator.asValue(from)
                    override implicit def fromValue(value: SourceValue): Option[Id] = idGenerator.asId(value)
                }
            }

            override protected def table = _table

            override val idKey: String = _table.id

            override protected def database: Database[String] = MixedDaoFactory.this.database
        }
    }

    implicit final protected def tableFor(tag: ClassTag[_]): Option[TableDef[_, _]] = {
        tables.get(tag.runtimeClass)
    }

    final override protected def idGenFor[Id: ClassTag, Model: ClassTag]: IdGenerator[Id] = {
        val table = tables(implicitly[ClassTag[Model]].runtimeClass)

        if (table.tableParent.isDefined) {
            val parent = table.tableParent.get
            idGenFor(parent.idType, parent.modelType).asInstanceOf[IdGenerator[Id]]
        } else super.idGenFor[Id, Model]
    }
}