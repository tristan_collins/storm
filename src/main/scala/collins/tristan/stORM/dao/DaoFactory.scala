package collins.tristan.stORM.dao

import collins.tristan.stORM.source._

import scala.collection.mutable
import scala.reflect.ClassTag

trait Database[SourceKey] {
    def get[Model: ClassTag](id: Any): Option[Model]
    def get[Model: ClassTag](query: Map[SourceKey, SourceValue]): List[Model]
}

protected[stORM] abstract class DaoFactory[SourceKey] {
    protected def sourceFactory: DataSourceFactory[SourceKey]

    final protected val database = new Database[SourceKey] {
        override def get[Model: ClassTag](id: Any): Option[Model] = {
            val modelClass = implicitly[ClassTag[Model]].runtimeClass

            if (daoMap.contains(modelClass)) {
                val dao = daoMap(modelClass).asInstanceOf[Dao[Any, Model]]
                dao(id)
            } else None
        }

        override def get[Model: ClassTag](query: Map[SourceKey, SourceValue]): List[Model] = {
            val modelClass = implicitly[ClassTag[Model]].runtimeClass

            if (daoMap.contains(modelClass)) {
                val dao = daoMap(modelClass).asInstanceOf[ModelDao[Any, Model]]
                dao.find(query.asInstanceOf[Map[String, SourceValue]])
            } else Nil
        }
    }

    private val daoMap = new mutable.HashMap[Class[_], Dao[_, _]]
    private val daoIdMap = new mutable.HashMap[Class[_], IdGenerator[_]]

    // Used to manage inter-model references
    private val generated = new mutable.HashMap[(Dao[_, _], Any), List[() => Unit]]
    private val accessed = new mutable.HashMap[(Dao[_, _], Any), List[() => Unit]]
    private val processing = new mutable.HashMap[Dao[_, _], List[Any]] // Model dao -> List of ids

    final def get[Id: ClassTag, Model: ClassTag]: Option[ModelDao[Id, Model]] = {
        val modelClass = implicitly[ClassTag[Model]].runtimeClass

        if (daoMap.contains(modelClass)) {
            Some(daoMap(modelClass).asInstanceOf[ModelDao[Id, Model]])
        } else {
            val idGen = idGenFor[Id, Model]
            val idSource = newIdSource[Id]
            val dataSource = sourceFactory.newSource[Model]

            val nDao = newDao[Id, Model](dataSource, idSource, idGen)

            if (nDao.isDefined) {
                val dao = nDao.get.asInstanceOf[ModelDao[Id, Model]]

                dao.OnModelAdded += processEvent[Id, Model](generated)
                dao.OnModelUpdated += processEvent[Id, Model](accessed)

                daoMap(modelClass) = dao
                daoIdMap(modelClass) = idGen

                Some(dao)
            } else None
        }
    }

    private def processEvent[T, V](map: mutable.HashMap[(Dao[_, _], Any), List[() => Unit]])
                                  (stop: () => Unit, args: (Dao[T, V], V)): Unit = {
        if (map.nonEmpty) {
            val modelDao = args._1
            val modelId = modelDao.idOf(args._2).get
            val mapKey = (modelDao, modelId)

            if (map.contains(mapKey)) {
                if (processing.contains(modelDao)) processing(modelDao) = processing(modelDao) :+ modelId
                else processing(modelDao) = List(modelId)

                val gen = map(mapKey)

                gen.foreach(_())

                map -= mapKey

                val p = processing(modelDao)

                if (p.isEmpty) processing.remove(modelDao)
                else processing(modelDao) = p.filterNot(_ == modelId)
            }
        }
    }

    private def newIdSource[Id: ClassTag]: ModelIdAccessor = {
        def addToMap(map: mutable.Map[(Dao[_, _], Any), List[() => Unit]], call: => Unit, src: Requester[_, _]): Unit = {
            if (!processing.contains(src.dao)) {
                val srcId = src.id
                val srcKey = (src.dao, srcId)

                val nList: List[() => Unit] = {
                    val f: () => Unit = call _

                    if (map.contains(srcKey)) map(srcKey) :+ f
                    else List(f)
                }
                map(srcKey) = nList
            }
        }

        new ModelIdAccessor {
            override def idOf[T: ClassTag](model: T, src: Requester[_, _]): Option[SourceValue] = {
                val modelClass = implicitly[ClassTag[T]].runtimeClass
                val modelDao = daoMap.get(modelClass).get.asInstanceOf[ModelDao[Any, T]]

                val id = {
                    val daoId = {
                        val id = modelDao.idOf(model)
                        if (id.isEmpty) {
                            val m = modelDao.find(model)
                            if (m.isDefined) Some(m.get.id)
                            else None
                        }
                        else id
                    }

                    val idGen = daoIdMap.get(modelClass).get.asInstanceOf[IdGenerator[Any]]

                    if (daoId.isDefined) {
                        addToMap(accessed, modelDao.update(daoId.get, model), src)
                        Some(idGen.asValue(daoId.get))
                    } else {
                        val id = idGen.gen

                        addToMap(generated, modelDao(id) = model, src)
                        Some(idGen.asValue(id))
                    }
                }

                id
            }

            override def modelWithId[T: ClassTag](id: SourceValue, src: Requester[_, _]): Option[T with Identifiable[_]] = {
                val modelClass = implicitly[ClassTag[T]].runtimeClass
                val modelDao = daoMap.get(modelClass).get.asInstanceOf[ModelDao[Any, T]]

                val modelId = daoIdMap.get(modelClass).get.asInstanceOf[IdGenerator[Any]].asId(id).get

                modelDao(modelId)
            }
        }
    }

    protected def newDao[Id: ClassTag, Model: ClassTag]
        (source: DataSource[SourceKey], idSource: ModelIdAccessor, idGenerator: IdGenerator[Id]): Option[ModelDao[Id, _]]

    protected def idGenFor[Id: ClassTag, Model: ClassTag]: IdGenerator[Id] = {
        val modelClass = implicitly[ClassTag[Model]].runtimeClass

        if (daoIdMap.contains(modelClass)) daoIdMap(modelClass).asInstanceOf[IdGenerator[Id]]
        else newIdGen[Id, Model]
    }

    protected def newIdGen[Id: ClassTag, Model: ClassTag]: IdGenerator[Id]
}

trait IdGenerator[Id]  {
    val converter: ValueConverter[Id]
    import converter._

    def gen: Id
    final def asValue(id: Id): SourceValue = id
    final def asId(value: SourceValue): Option[Id] = value
}