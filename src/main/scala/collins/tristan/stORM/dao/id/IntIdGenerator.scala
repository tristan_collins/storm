package collins.tristan.stORM.dao.id

import collins.tristan.stORM.converters.default.IntConverter
import collins.tristan.stORM.dao.IdGenerator
import collins.tristan.stORM.observer.Observable

final class IntIdGenerator(private var lastId: Int = 0) extends IdGenerator[Int] {
    val OnIdGenerated = new Observable[Int] {}

    def gen: Int = { lastId += 1; OnIdGenerated(lastId); lastId }

    override val converter = IntConverter
}