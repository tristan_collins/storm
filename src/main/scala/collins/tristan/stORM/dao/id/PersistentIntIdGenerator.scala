package collins.tristan.stORM.dao.id

import java.io.{File, PrintWriter}

import collins.tristan.stORM.converters.default.IntConverter
import collins.tristan.stORM.dao.IdGenerator

import scala.io.Source
import scala.reflect.ClassTag

final class PersistentIntIdGenerator(idFile: File) extends IdGenerator[Int] {
    private lazy val idGenerator = loadIdGen()

    override def gen: Int = idGenerator.gen

    private def loadIdGen(): IdGenerator[Int] = {
        val lastId = {
            if (idFile.exists()) {
                val src = Source.fromFile(idFile)
                val i = src.getLines().mkString.toInt
                src.close()
                i
            } else 0
        }

        val idGen = new IntIdGenerator(lastId)
        idGen.OnIdGenerated += onIdGenerated(idFile)

        idGen
    }

    private def onIdGenerated(idFile: File)(stop: () => Unit, id: Int): Unit = {
        if (idFile.exists) idFile.delete()
        idFile.createNewFile()

        val writer = new PrintWriter(idFile)
        writer.write(id.toString)
        writer.close()
    }

    override val converter = IntConverter
}

object PersistentIntIdGenerator {
    def apply[T: ClassTag](dir: File, ext: String = ".id"): PersistentIntIdGenerator = {
        new PersistentIntIdGenerator(new File(dir, implicitly[ClassTag[T]].runtimeClass.getSimpleName + ext))
    }
}