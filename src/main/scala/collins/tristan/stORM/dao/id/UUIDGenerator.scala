package collins.tristan.stORM.dao.id

import java.util.UUID

import collins.tristan.stORM.converters.UUIDConverter
import collins.tristan.stORM.dao.IdGenerator

final class UUIDGenerator extends IdGenerator[UUID] {
    def gen: UUID = UUID.randomUUID()

    override val converter = UUIDConverter
}
