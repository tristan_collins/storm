package collins.tristan.stORM.dao

import collins.tristan.stORM.observer.{CompositeObservable, Observable}
import collins.tristan.stORM.source.SourceValue

trait Identifiable[Id] {
     def id: Id
     def srcId: SourceValue
}

trait Dao[Id, Model] {
     type ListenerArgs = (Dao[Id, Model], IdModel)
     type IdModel = Model with Identifiable[Id]

     val OnModelAdded = new Observable[ListenerArgs] { }
     val OnModelRemoveStarted = new Observable[ListenerArgs] { }
     val OnModelRemoved = new Observable[ListenerArgs] { }
     val OnModelUpdated = new Observable[ListenerArgs] { }

     protected val RemovedComposite = CompositeObservable[Id, ListenerArgs]
     protected val UpdatedComposite = CompositeObservable[Id, ListenerArgs]

     def OnModelRemoved(id: Id): Observable[ListenerArgs] = RemovedComposite(id)
     def OnModelUpdated(id: Id): Observable[ListenerArgs] = UpdatedComposite(id)

     def apply(id: Id): Option[IdModel]
     def get(model: Model): Option[Model]
     def update(id: Id, model: Model): Option[IdModel]
     def update(model: Model): Boolean
     def update(model: IdModel): Option[IdModel]
     def idOf(model: Model): Option[Id]
     def getAll: List[IdModel]

     def +=(model: Model): IdModel
     def +=(model: Model, models: Model*): List[IdModel]
     def +=(data: Iterable[Model]): List[IdModel]
     def -=(data: Model*): List[IdModel]
     def -=(data: Iterable[Model]): List[IdModel]
 }
