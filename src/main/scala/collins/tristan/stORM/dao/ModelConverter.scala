package collins.tristan.stORM.dao

import java.lang.reflect.Method

import collins.tristan.stORM.source._
import net.sf.cglib.proxy._

import scala.reflect.ClassTag

/**
 * Created by Tristan on 15-Apr-15.
 */
protected[dao] abstract class ModelConverter[Id, Model: ClassTag] {
    def idKey: String
    protected def idAccessor: ModelIdAccessor
    protected def idConverter: ValueConverter[Id]
    protected def table: TableDef[Model, Id]
    protected def database: Database[String]

    private val modelClass = implicitly[ClassTag[Model]].runtimeClass

    protected def genIdAccessor(dao: Dao[Id, Model], id: Id, model: Model): IdAccessor = {
        new IdAccessor {
            val src = Requester(id, model, dao)
            override def idOf[T: ClassTag](model: T): Option[SourceValue] = idAccessor.idOf[T](model, src)
        }
    }

    private def genModelAccessor(model: Model, id: Id, dao: Dao[Id, Model]) = {
        new ModelAccessor {
            override def get[T: ClassTag](rId: SourceValue): Option[T with Identifiable[_]] = {
                idAccessor.modelWithId[T](rId, new Requester(id, model, dao.asInstanceOf[Dao[Any, Model]]))
            }
        }
    }

    final def model2Map(dao: ModelDao[Id, Model], id: Id, model: Model): Map[String, Option[SourceValue]] = {
        var modelMap = model2Map(model)

        modelMap = modelMap ++ table.foreignKeys.map(entry => {
            val key = entry._1
            val fk = entry._2

            key -> Some(fk.get(model)(genIdAccessor(dao, id, model)))
        })

        modelMap
    }

    /**
     * Used to map only the fields of the model (no foreign keys).
     * @param model Model to map
     * @return Map of fields
     */
    final def model2Map(model: Model): Map[String, Option[SourceValue]] = {
        var modelMap: Map[String, Option[SourceValue]] = table.columns.map(entry => {
            val key = entry._1
            val col = entry._2

            key -> Some(col.get(model))
        })

        modelMap = modelMap ++ table.optionalColumns.map(e => e._1 -> e._2.get(model))
            .map(e => e._1 -> e._2)

        modelMap
    }

    final def map2Model(dao: ModelDao[Id, Model], map: Map[String, Option[SourceValue]]): Model with Identifiable[Id] = {
        val model: Model with Identifiable[Id] = createLazyModel(map.get(idKey).get.get, lazyLoad(dao, map))
        model
    }

    def lazyLoad(dao: ModelDao[Id, Model],
                 map: Map[String, Option[SourceValue]])
                (model: Model, id: Id, srcId: SourceValue): Model = {
        table.columns.foreach({case (key, col) =>
            if (map.contains(key)) {
                val value = map(key)
                if (value.isDefined) col.set(model, value.get)
            }
        })

        table.optionalColumns.foreach({case (key, col) =>
            if (map.contains(key)) col.set(model, map(key))
        })

        table.foreignKeys.foreach({case (key, fk) =>
            if (map.contains(key)) {
                val value = map(key)
                if (value.isDefined) fk.set(model, value.get)(genModelAccessor(model, id, dao))
            }
        })

        table.foreignSets.foreach({case (key, foreignSet) =>
            foreignSet.set(model, srcId)(database, genIdAccessor(dao, id, model))
        })

        if (table.tableParent.isDefined) {
            val parent = genModelAccessor(model, id, dao).get(srcId)(table.tableParent.get.modelType)
            val callback = ReflectionHelper.getCallback[LazyModel[Id, Any]](parent.get)
            callback.get.modelLoader(model, id, srcId)
        }

        model
    }

    final private def createLazyModel(srcId: SourceValue,
                                      model: (Model, Id, SourceValue) => Model): Model with Identifiable[Id] = {
        val enhancer = new Enhancer
        enhancer.setSuperclass(modelClass)
        enhancer.setInterfaces(Array(classOf[Identifiable[Id]]))
        enhancer.setCallbacks(Array(
            new LazyModel[Id, Model](srcId, model, idConverter.fromValue(srcId).get, modelClass),
            NoOp.INSTANCE
        ))
        enhancer.setCallbackFilter(ReflectionHelper.FinalizeFilter)
        enhancer.setInterceptDuringConstruction(false)

        lazy val constructor = ReflectionHelper.getMinConstructor(modelClass)
        lazy val argTypes: Array[Class[_]] = constructor.getParameterTypes
        lazy val args = ReflectionHelper.getDefaultArgs(constructor)

        enhancer.create(argTypes, args).asInstanceOf[Model with Identifiable[Id]]
    }

    def wrapModel(value: Id, model: Model): Model with Identifiable[Id] = {
        val callback = ReflectionHelper.getCallback[LazyModel[Id, Model]](model)

        if (callback.isSuccess) {
            val lazyModel = callback.get
            val idField = lazyModel.getClass.getField("srcId")
            idField.setAccessible(true)
            idField.set(lazyModel, value)
            model.asInstanceOf[Model with Identifiable[Id]]
        } else {
            createLazyModel(value, (a, b, c) => model)
        }
    }

    implicit def id2Value(id: Id): SourceValue = idConverter.toValue(id)
}

private class LazyModel[Id, Model](val srcId: SourceValue,
                                             val modelLoader: (Model, Id, SourceValue) => Model,
                                             _id: => Id,
                                             modelClass: Class[_])
    extends DirtyProxy[Model] {
    private lazy val id = _id

    override def invoke(o: scala.Any, method: Method, args: Array[AnyRef]): AnyRef = {
        if (method.getName == "id") id.asInstanceOf[AnyRef]
        else if (method.getName == "srcId") srcId.asInstanceOf[AnyRef]
        else {
            super.invoke(o, method, args)
        }
    }

    private def createModel: Model = {
        try {
            modelClass.newInstance().asInstanceOf[Model]
        } catch {
            case e: Throwable =>
                val constructor = ReflectionHelper.getMinConstructor(modelClass)
                val args = ReflectionHelper.getDefaultArgs(constructor)

                constructor.newInstance(args: _*).asInstanceOf[Model]
        }
    }

    override protected def loadObj(): Model = {
        modelLoader(createModel, id, srcId)
    }
}