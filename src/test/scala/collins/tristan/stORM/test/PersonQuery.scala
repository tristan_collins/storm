package collins.tristan.stORM.test

import collins.tristan.stORM.converters.LocalDateConverter._
import collins.tristan.stORM.source.SourceValue
import org.joda.time.LocalDate

case class PersonQuery(name: Option[String] = None, dob: Option[LocalDate] = None)

object PersonQuery {
    def apply(name: String, dob: LocalDate): PersonQuery = PersonQuery(Some(name), Some(dob))
    def apply(name: String): PersonQuery = PersonQuery(Some(name))
    def apply(dob: LocalDate): PersonQuery = PersonQuery(None, Some(dob))

    implicit def query2Map(query: PersonQuery): Map[String, SourceValue] = {
        var map: Map[String, SourceValue] = Map()

        if (query.name.isDefined) map += (("name", query.name.get))
        if (query.dob.isDefined) map += (("dob", query.dob.get: SourceValue))

        map
    }
}