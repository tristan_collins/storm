package collins.tristan.stORM.test

import java.io.File

import collins.tristan.stORM.dao.id.PersistentIntIdGenerator
import collins.tristan.stORM.dao.{Dao, IdGenerator, ModelDao, SimpleDaoFactory}
import collins.tristan.stORM.source.{DataSourceFactory, SourceValue, ValueConverter}
import collins.tristan.stORM.sources.JSONSourceFactory
import collins.tristan.stORM.test.tables.{ChildTable, ParentTable, PersonTable}
import com.googlecode.lanterna.TerminalFacade
import com.googlecode.lanterna.gui.component.{Button, Label, Panel}
import com.googlecode.lanterna.gui.dialog.MessageBox
import com.googlecode.lanterna.gui.layout.VerticalLayout
import com.googlecode.lanterna.gui.listener.WindowListener
import com.googlecode.lanterna.gui.{Action, Interactable, Window}
import com.googlecode.lanterna.input.Key
import com.googlecode.lanterna.input.Key.Kind
import com.googlecode.lanterna.terminal.Terminal
import org.joda.time.{LocalDate, Period}

import scala.annotation.tailrec
import scala.reflect.ClassTag
import scala.util.Random

object IntConverter extends ValueConverter[Int] {
    override implicit def toValue(from: Int): SourceValue = SourceValue.int2Value(from)
    override implicit def fromValue(value: SourceValue): Option[Int] = SourceValue.parseValue(value)
}

object TestFactory extends SimpleDaoFactory[Int] {
    final override protected val sourceFactory: DataSourceFactory[String] = JSONSourceFactory()

    register(PersonTable)
    register(ParentTable)
    register(ChildTable)

    override protected def newIdGen[Model: ClassTag]: IdGenerator[Int] = {
        val idFile = new File("data", implicitly[ClassTag[Model]].runtimeClass.getSimpleName + ".id")
        new PersistentIntIdGenerator(idFile)
    }
}

object main {
    def formatPeriod(period: Period): String = {
        @tailrec
        def format(period: Period, strings: List[String]): List[String] = {
            if (period.getYears > 0) {
                format(period.minusYears(period.getYears), strings :+ s"${period.getYears} years")
            } else if (period.getMonths > 0) {
                format(period.minusMonths(period.getMonths), strings :+ s"${period.getMonths} months")
            } else {
                strings
            }
        }

        if (period.getDays == 0) "Just born"
        else format(period, Nil).mkString(" ")
    }

    def formatPerson(pFormatter: Period => String)(p: Person): String = {
        s"name=${p.name}, current age=${pFormatter(p.age(LocalDate.now))}"
    }

    def formatModel[T: ClassTag](mFormatter: (T) => String): T => String = (m) => {
        val id = TestFactory.get[T].get.idOf(m)

        if (id.isDefined) s"id=${id.getOrElse("No Id")} ${mFormatter(m)}"
        else mFormatter(m)
    }

    /*def formatSubPerson(p: SubPerson, pFormatter: Period => String): String = {
        s"x=${p.x} ${formatPerson(p, pFormatter)}"
    }*/

    def formatQueryResult[Id, Model](dao: ModelDao[Id, Model])
                                           (singular: String, plural: String)
                                           (modelFormatter: (Model) => String)
                                           (query: Map[String, SourceValue], indentCount: Int = 1, indentString: String = "\t") = {
        def newLine(str: String = ""): String = str + "\n" + indentString * indentCount

        val found = dao.find(query)
        s"${found.length} ${if (found.length == 1) singular else plural} found${newLine()}${found.map(p => modelFormatter(p)).mkString(newLine())}"
    }

    def onModelAdded(stop: () => Unit, args: (Dao[Int, Person], Person)) {
        val dao = args._1
        val id = dao.idOf(args._2).get

        println(s"${"-"*5}Person added: " + id)
        println(s"${"-"*5}Added update and remove listener")

        val stopUpdate = dao.OnModelUpdated(id) += onModelUpdated
        dao.OnModelRemoved(id) += onModelRemoved(stopUpdate)
    }

    def onModelUpdated(stop: () => Unit, args: (Dao[Int, Person], Person)) {
        val pFormatter = formatModel(formatPerson(formatPeriod))

        println(s"${"*"*2}Updated ${args._1.idOf(args._2)}}")
        println(s"${"*"*3}${pFormatter(args._2)}")
    }

    def onModelRemoved(stopUpdate: () => Unit)(stopRem: () => Unit, args: (Dao[Int, Person], Person)) {
        stopUpdate()
        stopRem()

        println(s"${"*"*2}Person removed: " + args._1.idOf(args._2))
    }

    def main(args: Array[String]): Unit = {
        //testSingleReference()
        //testMultiReference()
        test()

        //textUICurses()
        //textUILanterna()
    }

    def test(): Unit = {
        import ModelDao._

        val parents = TestFactory.get[Int, Parent].get
        val children = TestFactory.get[Child].get

        val parent = parents += Parent("Foo", 41, "Joe").haveChild("Bob").haveChild("Bar")

        val fooChildren = children.find(ChildTable.parent EQUALS parent.id)
        println(s"${parent.name} has ${fooChildren.length} children")

        parents.OnModelRemoved += {case (stop, args) =>
            println(s"Parent '${args._2.id}' has died")
        }

        children.OnModelRemoved += {case (stop, args) =>
            println(s"Child '${args._2.id}' has died")
        }

        parents -= parent
    }

    def textUILanterna(): Unit = {
        val screen = TerminalFacade.createScreen()
        val gui = TerminalFacade.createGUIScreen(screen)
        screen.startScreen()

        val mainWindow = new Window("Hello World")

        val panel = new Panel()
        panel.setLayoutManager(new VerticalLayout)

        val label = new Label("", Terminal.Color.GREEN)

        val clickMe = new Button("Click me", new Action() {
            override def doAction(): Unit = MessageBox.showMessageBox(gui, "Clicked", "You clicked me!")
        })

        panel.addComponent(label)
        panel.addComponent(clickMe)

        mainWindow.addWindowListener(new WindowListener {
            override def onUnhandledKeyboardInteraction(window: Window, key: Key): Unit = {
                key.getKind match {
                    case Kind.Escape => screen.stopScreen(); System.exit(0)
                    case Kind.NormalKey => label.setText(s"${label.getText}${key.getCharacter}")
                    case _ =>
                }
            }

            override def onWindowClosed(window: Window): Unit = {}
            override def onFocusChanged(window: Window, interactable: Interactable, interactable1: Interactable): Unit = {}
            override def onWindowShown(window: Window): Unit = {}
            override def onWindowInvalidated(window: Window): Unit = {}
        })

        mainWindow.addComponent(panel)
        gui.showWindow(mainWindow)
    }

    /*def textUICurses(): Unit = {
        val window = new CWindow(0, 0, 1280, 720, true, "Hello World")
        window.setBorderColors(new CharColor(CharColor.MAGENTA, CharColor.YELLOW))

        val man = new DefaultLayoutManager
        man.bindToContainer(window.getRootPanel)

        val (btnX, btnY) = (10, 10)
        val clickMeBtn = new Button("Click me for stuff")
        clickMeBtn.setShortCut('d')
        clickMeBtn.addListener(new ActionListener {
            override def actionPerformed(event: ActionEvent): Unit = {
                val (width, height) = (20, 20)
                val (x, y) = (btnX - width / 2, btnY - width / 2)
                val dlg = new Dialog(x, y, width, height, true, "Clicked")
                dlg.setShadow(true)

                val dlgMan = new GridLayoutManager(width - 2, height - 2)
                //dlgMan.bindToContainer(dlg.getRootPanel)
                dlg.getRootPanel.setLayoutManager(dlgMan)

                val btnClose = new Button("Close")
                btnClose.setShortCut('c')
                btnClose.addListener(new ActionListener {
                    override def actionPerformed(event: ActionEvent): Unit = dlg.close()
                })
                dlgMan.addWidget(btnClose, 0, 0, 10, 10, height - 2, width - 2)

                dlg.show()
            }
        })
        man.addWidget(clickMeBtn, btnX, btnY, 100, 20, 20, 400)

        window.show()
    }*/

    def testMultiReference(): Unit = {
        def formatName(n: {def name: String}): String = {
            s"name=${n.name}"
        }

        def formatParent(p: Parent): String = {
            s"${formatName(p)}, age=${p.age}, child=${p.child.name}"
        }

        def formatChild(c: Child): String = formatName(c)

        val pFormatter = formatModel(formatParent)
        val cFormatter = formatModel(formatChild)

        val (pDao, cDao) = (TestFactory.get[Parent].get, TestFactory.get[Int, Child].get)

        pDao.OnModelAdded += ((stop: () => Unit, args: (Dao[Any, Parent], Parent)) => {
            println(s"Added parent:\n\t${pFormatter(args._2)}")
        })

        pDao.OnModelRemoved += ((stop: () => Unit, args: (Dao[Any, Parent], Parent)) => {
            println(s"${args._2.name} has died")
        })

        cDao.OnModelAdded += ((stop: () => Unit, args: (Dao[Int, Child], Child)) => {
            println(s"Added child:\n\t${cFormatter(args._2)}")
        })

        cDao.OnModelRemoved += ((stop: () => Unit, args: (Dao[Int, Child], Child)) => {
            println(s"${args._2.name} has died")
        })

        val parent = {
            val parents = pDao.getAll

            if (parents.nonEmpty) parents.head
            else pDao += Parent("Foo", 23, "Bar")
        }

        cDao += Child(parent, "Bastard")

        val p = pDao(parent.id).get
        println(s"parent.child.id=${cDao.idOf(p.child)}")
        println(s"parent has ${p.children.size} children")

        println(s"children: ${p.children.map(_.name).mkString(", ")}")

        {
            println(s"${p.name} is having a child")
            val cName = Random.nextString(5)
            val nParent = p.haveChild(cName)
            println(s"${p.name} gave birth to '$cName'")
            pDao(p.id) = nParent
            println(s"${p.name} has ${pDao(p.id).get.children.size} children")
            pDao -= nParent
        }
    }

    def testSingleReference(): Unit = {
        val pFormatter = formatModel(formatPerson(formatPeriod))
        val pBuilder = new Person.Builder("1st Built")
        val subBuilder = new SubPerson.Builder
        val pDao = TestFactory.get[Person].get.asInstanceOf[ModelDao[Int, Person]]

        val addStop = pDao.OnModelAdded += onModelAdded

        val people = Array(
            Person("Someone", new LocalDate(1983, 4, 12)),
            Person("Joe", new LocalDate(1964, 10, 21)),
            Person("Foo", new LocalDate(1990, 10, 4)),
            Person("Bar", new LocalDate(1994, 4, 23)),
            Person("Foo", new LocalDate(1893, 12, 30)),
            new Person("Bob", LocalDate.now),
            pBuilder.build,
            pBuilder.setName("2nd Built").build,
            subBuilder.setName("1st Sub Person").build)

        println(s"before=${pDao.getAll.size}")
        val added = pDao += people
        println(s"after=${pDao.getAll.size}")
        added.foreach(m => println(s"${pDao.idOf(m)}: ${pFormatter(m)}"))

        def queryFormatter = formatQueryResult(pDao)("person", "people")(pFormatter) _
        println(s"find(name=Foo):\n\t${queryFormatter(PersonQuery(name="Foo"), 2, "\t")}")
        println(s"find(name=Mary Joe):\n\t${queryFormatter(PersonQuery(name="Mary Joe"), 2, "\t")}")
        println(s"find(dob=1964-10-21):\n\t${queryFormatter(PersonQuery(new LocalDate(1964, 10, 21)), 2, "\t")}")

        val removed = pDao -= PersonQuery("Foo")
        println(s"removed ${removed.length}:\n\t${removed.map(p => pFormatter(p)).mkString("\n\t")}")
        println(s"remove(name=Foo):\n\t${pDao.getAll.map(p => pFormatter(p)).mkString("\n\t")})}")

        pDao(pDao.idOf(people(0)).get) = Person("Updated person", LocalDate.now.minusYears(23))

        addStop()
    }
}
