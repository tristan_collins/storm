package collins.tristan.stORM.test

import scala.util.Random

sealed class Parent(val name: String, val age: Int, private val childName: String) {
    val child: Child = Child(this, childName)
    val children: Set[Child] = Set()

    protected[this] def this() = this("", 0, "")

    private def childGetter(p: Parent) = p.children
    private def copy(name: String = name,
                     age: Int = age,
                     child: Child = child,
                     children: Parent => Set[Child] = childGetter): Parent = {
        val pChild = child
        val pChildren = children

        new Parent(name, age, child.name) {
            lazy val _children = pChildren(this)

            override val child: Child = pChild
            override val children: Set[Child] = _children
        }
    }

    def haveChild(name: String): Parent = copy(children = children + Child(_, name))
}

object Parent {
    def apply(name: String, age: Int, childName: String): Parent = new Parent(name, age, childName)
}

case class Child(parent: Parent, name: String, rand: Double = Random.nextDouble(), randF: Float = Random.nextFloat()) {
    protected[this] def this() = this(null, "")
}