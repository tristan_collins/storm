package collins.tristan.stORM.test

import collins.tristan.stORM.test.Person.BuilderTrait
import org.joda.time.LocalDate

/**
 * Created by TrImAn on 23-Mar-15.
 */
class SubPerson(_name: String, _dob: LocalDate, val x: Int) extends Person(_name, _dob) {
    private def this() = this("", LocalDate.now, 0)
}

object SubPerson {
    case class Builder(x: Int = 23, name: String = "Builder") extends BuilderTrait {
        type BuilderType = Builder
        override type PersonType = SubPerson

        override def build: PersonType = new SubPerson(name, LocalDate.now.plusYears(2), x)

        override def setName(name: String): BuilderType = copy(x, name)

        def setX(x: Int): BuilderType = copy(x, name)
    }
}