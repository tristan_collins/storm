package collins.tristan.stORM.test

import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.geometry.Insets
import scalafx.scene.Scene
import scalafx.scene.control.Label
import scalafx.scene.layout.VBox

object fxmain extends JFXApp {
    stage = new PrimaryStage {
        scene = new Scene {
            root = new VBox {
                padding = Insets(25)
                content = Seq(
                    new Label("Hello World")
                )
            }
        }
    }
}
