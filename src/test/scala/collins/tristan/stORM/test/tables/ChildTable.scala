package collins.tristan.stORM.test.tables

import collins.tristan.stORM.converters.default._
import collins.tristan.stORM.source.TableDef
import collins.tristan.stORM.test.Child

object ChildTable extends TableDef[Child, Int] {
    val parent = foreignKey("parent", ParentTable)
    val name = map (column[String] to "name")
    val rand = map (column[Double] to "rand")
    val randF = map (column[Float] to "randF")
}
