package collins.tristan.stORM.test.tables

import collins.tristan.stORM.converters.default._
import collins.tristan.stORM.source.TableDef
import collins.tristan.stORM.test.Parent

object ParentTable extends TableDef[Parent, Int] {
    val name = map (column[String] to "name")
    val age = map (column[Int] to "age")
    val child = foreignKey("child", ChildTable)

    val children = oneToMany("children", ChildTable)(_.parent)
}
