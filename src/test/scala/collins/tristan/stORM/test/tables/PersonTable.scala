package collins.tristan.stORM.test.tables

import collins.tristan.stORM.converters.LocalDateConverter
import collins.tristan.stORM.converters.default._
import collins.tristan.stORM.source.TableDef
import collins.tristan.stORM.test.Person
import org.joda.time.LocalDate

object PersonTable extends TableDef[Person, Int] {
    val name = map (column[String] to "_name")
    val dob = map (column[LocalDate] to "_dob")
}