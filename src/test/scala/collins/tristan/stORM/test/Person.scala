package collins.tristan.stORM.test

import java.util.UUID

import org.joda.time.{LocalDate, Period}

/**
 * Created by TrImAn on 23-Mar-15.
 */
class Person(private val _name: String, private val _dob: LocalDate) {
    def name = _name
    def dateOfBirth = _dob

    protected[this] def this() = this("", LocalDate.now)

    def age(date: LocalDate) = {
        new Period(dateOfBirth, date)
    }
}

object Person {
    trait BuilderTrait {
        type BuilderType
        type PersonType

        val name: String

        def setName(name: String): BuilderType
        def build: PersonType
    }

    case class Builder(name: String = "") extends BuilderTrait {
        type BuilderType = Builder
        type PersonType = Person

        final def setName(name: String) = copy(name)
        def build: PersonType = new Person(name, LocalDate.now)
    }

    private def uuidGen = UUID.randomUUID

    def apply(name: String, dob: LocalDate) = new Person(name, dob)
    //def apply[T <: {def nextId: Int}](name: String, dob: LocalDate)(implicit idGen: T) = new Person(idGen.nextId)(name, dob)
}